# Contributing to the Democracy Open Source Code

Thank you for considering contributing to the Democracy Data project.
Whether adding a single line of code, or contributing years of coding,
it’s our combined effort that makes open source projects like this
not only possible, but potentially great.

Following these guidelines helps to communicate that you respect the time of the developers
managing and developing this open source project. In return, they should reciprocate that respect
in addressing your issue, assessing changes, and helping you finalize your pull requests.

## What to contribute?

There are many ways to contribute, from writing tutorials or blog posts, improving the
documentation, submitting bug reports and feature requests or writing code.

### Where to start in the code?

If you’re new to the project, you can start by looking through the issues to see some of the areas
that need attention.

You can also pull the “TODOs” out of the code itself by running:
    bundle exec rails notes
from the project root directory (e.g., a path like `~/projects/democracy/`).
Those are points in the code where the writer had an idea of something that would be useful,
but didn’t get around to implementing it (yet).

## Code requirements?

To try to have all of our contributions work well together, please follow these guidelines
in your coding work for the project:

* Before embarking on any major changes or additions, please check the project *issues*
  and create a new issue if none match what you are trying to do.
  You are strongly encouraged to discuss your design plan before getting deep into coding,
  to make sure that it aligns with the project direction.
* *Test* all the things! All code in the project must have 100% test coverage before being accepted.
* Please run the *preflight* tools against any code you intend to contribute.
  At a minimum, all code is required to completely pass Rubocop inspection.
  `bundle exec rubocop --config config/rubocop.yml --format simple --out reports/rubocop.txt`
* Create a new feature *branch* for your changes.

*Important:* By contributing code to this project, you are agreeing to release your rights to the
code under the terms of the [LICENSE](./LICENSE).

## New to open source coding?

Here are a few tutorials and introductory explanations that you may find useful:

* [First Timers Only](http://www.firsttimersonly.com/)
* [Make a Pull Request](http://makeapullrequest.com/)

## How to report a bug?

*Critical:* If you find a *security* vulnerability, please do NOT open an issue.
Email activist@gmail.com instead, please.
(If you are using encryption: [GPG/PGP key](http://grantneufeld.ca/grantneufeld-publickey.asc).)

For bugs that are not security related, please post an *Issue* explaining:
1. What is the sequence of actions that produces the bug?
2. What were you expecting to have happen?
3. What actually happened?

## How to suggest a feature or change?

Chances are that if you wish this project had some functionality it currently doesn’t that someone
else would like that, too.

You can help by opening an Issue where you describe, with as many specifics as you can, what it is
you would like to have added. Please also explain why it would be useful to you — what is the
problem it would solve.
