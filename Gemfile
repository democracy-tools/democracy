source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.3'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# User Access Control
gem 'devise'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  # specs/testing:
  gem 'factory_girl_rails'
  gem 'rspec', require: false
  gem 'rspec-rails', require: false
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # continuous testing:
  gem 'guard', require: false
  gem 'guard-rspec', require: false
  # mutation testing
  gem 'mutant-rspec', require: false # https://github.com/mbj/mutant
  # gem validation
  gem 'bundler-audit', require: false # https://github.com/postmodern/bundler-audit
  # code analysis tools:
  gem 'brakeman', require: false # http://brakemanscanner.org/
  gem 'rails_best_practices', require: false # https://github.com/railsbp/rails_best_practices
  gem 'rubocop', require: false # https://github.com/bbatsov/rubocop
  gem 'rubycritic', require: false # https://github.com/whitesmith/rubycritic
end

group :test do
  # additional testing functions
  gem 'capybara', require: false # browser interaction simulation
  gem 'html_validation', require: false # relies on the tidy command-line tool to validate html
  gem 'rails-controller-testing'
  gem 'rspec-activemodel-mocks', require: false
  # code test coverage analysis:
  gem 'simplecov', require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
