# Democracy Data (probably need a more distinct name)

A web content management system (CMS) for operating websites focused on
tracking governmental and political information; such as elections, candidates,
political parties, platforms, governments, government officials, issues,
policies, legislation, and laws.

This web-based application is being developed specifically for the
[Calgary Democracy Project](http://calgarydemocracy.ca/),
with the intent to eventually serve as a broader platform for supporting
democratic engagement and information sharing.

Co-ordinator: [Grant Neufeld](http://grantneufeld.ca/)

Copyright: © 2010-2017 Grant Neufeld.
