require 'access/authority_check'
require 'page_metadata'

# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.
#
# Sub-classes should define the `area` instance method.
# If access is to be restricted based on Authorities,
# it should return a string for the authority area (e.g., 'system', 'contact', etc.).
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  helper_method :authority?
  helper_method :page_metadata

  rescue_from ActiveRecord::RecordNotFound, with: :missing
  rescue_from Wayground::AccessDenied, with: :unauthorized
  rescue_from Wayground::LoginRequired, with: :login_required

  protected

  # report that the requested url does not exist (missing - 404 error)
  # TODO: support params for missing (such as name of missing resource, e.g., "Group ID 123")
  def missing
    page_metadata(title: '404 Missing', nocache: true)
    flash.now.alert = 'Requested resource not found.'
    render template: 'routing/missing', status: '404 Missing'
  end

  # report that the user is not authorized
  def unauthorized
    page_metadata(title: 'Unauthorized', nocache: true)
    flash.now.alert = 'You are not authorized for accessing the requested resource'
    render template: 'routing/unauthorized', status: '403 Forbidden'
  end

  # report that the user must sign in
  def login_required
    page_metadata(title: 'Sign In Required', nocache: true)
    flash.now.alert = 'You must sign in to access the requested resource'
    render template: 'routing/login_required', status: '401 Unauthorized'
  end

  def page_metadata(params = {})
    if @page_metadata
      @page_metadata.merge_params(params)
    else
      @page_metadata = Wayground::PageMetadata.new(params)
    end
    @page_metadata
  end

  # Devise configuration

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
  end

  # Access Authorization Checks

  def check_can_read
    @authority = requires_authority(area, :can_read)
  end

  def check_can_create
    @authority = requires_authority(area, :can_create)
  end

  def check_can_update
    @authority = requires_authority(area, :can_update)
  end

  def check_can_destroy
    @authority = requires_authority(area, :can_destroy)
  end

  def requires_authority(area, action = :can_read)
    user = current_user
    raise Wayground::LoginRequired unless user
    checker = Wayground::Access::AuthorityCheck.new(user: user, area: area, action: action)
    authority = checker.authority
    raise Wayground::AccessDenied unless authority
    authority
  end

  def authority?(area, action = :can_read)
    user = current_user
    return nil unless user
    checker = Wayground::Access::AuthorityCheck.new(user: user, area: area, action: action)
    checker.authority
  end
end
