require 'authority'

# Access to the Authority model
class AuthoritiesController < ApplicationController
  before_action :check_can_read, only: %i[index show edit]
  before_action :check_can_create, only: %i[new create]
  before_action :check_can_update, only: %i[edit update]
  before_action :check_can_destroy, only: %i[delete destroy]
  before_action :set_authority, only: %i[show edit update delete destroy]
  before_action :set_new_authority, only: %i[new create]

  def index
    @authorities = Authority.all
  end

  def show; end

  def new; end

  def create
    if @authority.save
      redirect_to(@authority, notice: 'Authority was successfully created.')
    else
      flash.now.alert = 'Could not save the authority with the given values.'
      render action: 'new'
    end
  end

  def edit; end

  def update
    if @authority.update_attributes(authority_params)
      redirect_to(@authority, notice: 'Authority was successfully updated.')
    else
      flash.now.alert = 'Could not update the authority with the given values.'
      render action: 'edit'
    end
  end

  def delete; end

  def destroy
    @authority.destroy
    redirect_to(authorities_url, notice: 'Authority was deleted.')
  end

  protected

  def area
    'system'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_authority
    @authority = Authority.find(params.fetch(:id))
  end

  def set_new_authority
    @authority = Authority.new(authority_params)
    # may receive related ids as simple params rather than values on the authority param hash
    @authority.user_id ||= params[:user_id]
    @authority.role_id ||= params[:role_id]
    @authority.active_at ||= Time.zone.now
  end

  def authority_params
    params.fetch(:authority, {}).permit(
      :user_id, :role_id, :active_at, :ends_at, :suspends_at, :unsuspends_at
    )
  end
end
