# Manage “static” (web/html) Pages.
class PagesController < ApplicationController
  before_action :check_can_read, only: %i[index show edit]
  before_action :check_can_create, only: %i[new create]
  before_action :check_can_update, only: %i[edit update]
  before_action :check_can_destroy, only: %i[delete destroy]
  before_action :set_page, only: %i[show edit update delete destroy]
  before_action :set_parent

  def index
    @pages = Page.all
  end

  def show; end

  def new
    @page = Page.new(parent: @parent)
  end

  def create
    @page = Page.new(page_params)
    if @page.save
      redirect_to @page, notice: 'Page was successfully created.'
    else
      flash.now.alert = 'Could not save the page with the given values.'
      render :new
    end
  end

  def edit; end

  def update
    if @page.update(page_params)
      redirect_to @page, notice: 'Page was successfully updated.'
    else
      flash.now.alert = 'Could not update the page with the given values.'
      render :edit
    end
  end

  def delete; end

  def destroy
    @page.destroy!
    redirect_to pages_url, notice: 'Page was successfully deleted.'
  end

  protected

  def area
    'page'
  end

  private

  def set_page
    @page = Page.find(params[:id])
  end

  def set_parent
    @parent = @page&.parent
    @parent = parent_from_parent_id unless @parent
    @parent = parent_from_page_parent_id unless @parent
  end

  def parent_from_parent_id
    parent_id = params[:parent_id]
    Page.find(parent_id) if parent_id
  rescue ActiveRecord::RecordNotFound
    nil
  end

  def parent_from_page_parent_id
    parent_id = params[:page]
    parent_id = parent_id[:parent_id] if parent_id
    Page.find(parent_id) if parent_id
  rescue ActiveRecord::RecordNotFound
    nil
  end

  def page_params
    params.require(:page).permit(:parent_id, :filename, :title, :description, :content)
  end
end
