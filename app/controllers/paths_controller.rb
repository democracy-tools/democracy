# Defines arbitrary url paths for accessing items and doing redirects.
class PathsController < ApplicationController
  before_action :check_can_read, only: %i[index show edit]
  before_action :check_can_create, only: %i[new create]
  before_action :check_can_update, only: %i[edit update]
  before_action :check_can_destroy, only: %i[delete destroy]
  before_action :set_path, only: %i[show edit update delete destroy]

  def index
    @paths = Path.all
  end

  def show; end

  def new
    @path = Path.new
  end

  def create
    @path = Path.new(path_params)
    if @path.save
      redirect_to @path, notice: 'Path was successfully created.'
    else
      flash.now.alert = 'Could not save the path with the given values.'
      render :new
    end
  end

  def edit; end

  def update
    if @path.update(path_params)
      redirect_to @path, notice: 'Path was successfully updated.'
    else
      flash.now.alert = 'Could not update the path with the given values.'
      render :edit
    end
  end

  def delete; end

  def destroy
    @path.destroy!
    redirect_to paths_url, notice: 'Path was successfully deleted.'
  end

  protected

  def area
    'page'
  end

  private

  def set_path
    @path = Path.find(params[:id])
  end

  def path_params
    params.require(:path).permit(:item_type, :item_id, :sitepath, :redirect)
  end
end
