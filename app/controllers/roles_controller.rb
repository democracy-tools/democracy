require 'role'

# Access to the Role model
class RolesController < ApplicationController
  before_action :check_can_read, only: %i[index show edit]
  before_action :check_can_create, only: %i[new create]
  before_action :check_can_update, only: %i[edit update]
  before_action :check_can_destroy, only: %i[delete destroy]
  before_action :set_role, only: %i[show edit update delete destroy]

  def index
    @roles = Role.all
  end

  def show; end

  def new
    @role = Role.new
  end

  def create
    @role = Role.new(role_params)
    if @role.save
      redirect_to @role, notice: 'Role was successfully created.'
    else
      flash.now.alert = 'Could not save the role with the given values.'
      render :new
    end
  end

  def edit; end

  def update
    if @role.update(role_params)
      redirect_to @role, notice: 'Role was successfully updated.'
    else
      flash.now.alert = 'Could not update the role with the given values.'
      render :edit
    end
  end

  def delete; end

  def destroy
    @role.destroy
    redirect_to roles_url, notice: 'Role was deleted.'
  end

  protected

  def area
    'system'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_role
    @role = Role.find(params.fetch(:id))
  end

  # Only allow a trusted parameter "white list" through.
  def role_params
    trusted = params.require(:role).permit(
      :filename, :name, :description,
      :can_read, :can_create, :can_update, :can_destroy, :can_upload, :can_comment,
      areas: []
    )
    # strip any blank area values
    trusted[:areas]&.delete_if(&:blank?)
    trusted
  end
end
