require 'setup/installer'

# Handle the display of requests based on arbitrary paths,
# including the special cases of the root and about paths.
class RoutingController < ApplicationController
  before_action :only_if_no_user_records, only: %i[fresh_install create_initial_records]

  def root
    if User.count.zero?
      render action: 'fresh_install'
    else
      @sitepath = '/'
      # use the custom root item if there is one; otherwise the default root view
      path_action if load_path
    end
  end

  def about
    @sitepath = '/about'
    # use the custom about item if there is one; otherwise the default about view
    path_action if load_path
  end

  # process arbitrary paths
  def sitepath
    @sitepath = params[:url].to_s
    path = load_path
    if path
      path_action
    else
      missing
    end
  end

  def fresh_install; end

  def create_initial_records
    installer = Wayground::Setup::Installer.new(params)
    @initial_records = installer.create_initial_records
    redirect_to(root_url, notice: 'Installed initial administrative records.')
  rescue ActiveRecord::RecordInvalid
    flash.now.alert = 'One or more of the field entries were invalid.'
    @user = installer.record_list[:user]
    render action: 'fresh_install'
  end

  protected

  def only_if_no_user_records
    # only allow if no user records exist
    raise Wayground::AccessDenied if User.count.positive?
  end

  private

  def load_path
    @path ||= Path.find_for_sitepath(@sitepath)
  end

  def path_action
    if @path.redirect?
      redirect_to @path.redirect
    else
      render_path_item(@path.item)
    end
  end

  def render_path_item(item)
    if item.is_a? Page
      render_item_as_page(item)
    # TODO: handle use of Paths for items other than Pages
    # elsif item.is_a? ???
    else
      render template: 'routing/unimplemented', status: '501 Not Implemented'
    end
  end

  def render_item_as_page(item)
    @page = item
    page_metadata(title: @page.title, description: @page.description)
    render template: 'routing/page'
  end
end
