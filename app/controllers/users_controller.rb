# Creation and management of Users.
class UsersController < ApplicationController
  before_action :check_can_read, only: %i[index show edit unlock_form]
  before_action :check_can_create, only: %i[new create]
  before_action :check_can_update, only: %i[edit update unlock_form unlock]
  before_action :check_can_destroy, only: %i[delete destroy]
  before_action :set_user, only: %i[show edit update delete destroy unlock_form unlock]

  def index
    @users = User.all
  end

  def show; end

  def new
    @user = User.new
  end

  def edit; end

  def create
    user_params = params.fetch(:user, {}).permit(:name, :email, :password, :password_confirmation)
    @user = User.new(user_params)
    if @user.save
      redirect_to user_url(@user), notice: 'User was successfully created.'
    else
      flash.now.alert = 'Could not save the user with the given values.'
      render :new
    end
  end

  def update
    if @user.update(params.fetch(:user, {}).permit(:name))
      redirect_to user_url(@user), notice: 'User was successfully updated.'
    else
      flash.now.alert = 'Could not update the user with the given values.'
      render :edit
    end
  end

  def delete; end

  def destroy
    @user.destroy
    redirect_to users_url, notice: 'User was deleted.'
  end

  def unlock_form; end

  def unlock
    # TODO: raise exception if trying to unlock a user that isn’t locked
    @user.unlock_access!
    redirect_to user_url(@user), notice: 'User was successfully unlocked.'
  end

  protected

  def area
    'system'
  end

  private

  def set_user
    @user = User.find(params.fetch(:id))
  end
end
