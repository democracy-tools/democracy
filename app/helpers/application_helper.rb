# The top level helper.
# Methods here will be available to all views.
module ApplicationHelper

  # Generate the errors report to show with a form.
  # item: the item to get the errors from
  # heading: the optional error message, shown as a heading above the list of errors
  def show_errors(item, heading = nil)
    render partial: 'layouts/errors', locals: { item: item, heading: heading }
  end

  # Must have either a section or an item — not both.
  # section - the current section (e.g., 'user', 'workflow_templates';
  # item - the active item
  # associated - items associated with the active item (if any).
  #         an array of: ActiveRecord type objects,
  #         or {descriptor:, link:} hashes.
  # actions - optional hash of {key: link}
  #           where key is an action like read, create; and link is the link/path to the action
  def wayfinder(section: nil, item: nil, associated: nil, actions: nil)
    @wayfinder = { section: section, item: item, associated: associated, actions: actions }
  end

  # Given an item, get it’s classname in lowercase, underscored, format.
  # E.g., ExampleThing becomes 'example_thing'
  def css_classerize(thing)
    thing.class.name.underscore.dasherize
  end

end
