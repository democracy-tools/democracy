# Abstract root class for mailers.
class ApplicationMailer < ActionMailer::Base
  default from: 'open@calgarydemocracy.ca'
  layout 'mailer'
end
