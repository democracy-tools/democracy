# Abstract root class for models
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  protected

  # Validation Helpers

  def ends_before_starts?(start_on, end_on)
    start_on && end_on && start_on > end_on
  end

  def ends_on_or_before_starts?(start_on, end_on)
    start_on && end_on && start_on >= end_on
  end
end
