# Joins a User to a Role, for a Business, to define access permissions.
class Authority < ApplicationRecord
  belongs_to :user
  belongs_to :role

  validates :active_at, presence: true
  validate :must_not_end_before_started
  validate :must_not_suspend_before_started
  validate :must_not_unsuspend_before_suspends

  # ends_at must be after active_at
  def must_not_end_before_started
    err_msg = 'Must be after the date and time it becomes active at'
    errors.add(:ends_at, err_msg) if ends_on_or_before_starts?(active_at, ends_at)
  end

  # suspends_at must be on or after active_at
  def must_not_suspend_before_started
    err_msg = 'Must be on or after the date and time it becomes active at'
    errors.add(:suspends_at, err_msg) if ends_before_starts?(active_at, suspends_at)
  end

  # unsuspends_at must be after suspends_at
  def must_not_unsuspend_before_suspends
    err_msg = 'Must be after the date and time it suspends at'
    errors.add(:unsuspends_at, err_msg) if ends_on_or_before_starts?(suspends_at, unsuspends_at)
  end

  scope :active, lambda {
    where(
      'active_at <= :now ' \
      'AND (ends_at IS NULL OR ends_at > :now) ' \
      'AND (' \
      'suspends_at IS NULL OR suspends_at > :now ' \
      'OR (unsuspends_at IS NOT NULL AND unsuspends_at <= :now)' \
      ')',
      now: Time.zone.now
    )
  }

  scope :expired, lambda {
    where('active_at > :now OR (ends_at IS NOT NULL AND ends_at <= :now) ', now: Time.zone.now)
  }

  scope :suspended, lambda {
    where(
      '(suspends_at IS NOT NULL AND suspends_at <= :now) ' \
      'AND (unsuspends_at IS NULL OR unsuspends_at > :now)',
      now: Time.zone.now
    )
  }

  def active?
    now = Time.zone.now
    has_activated = active_at <= now
    has_not_ended = ends_at.nil? || ends_at > now
    has_activated && has_not_ended && !suspended?
  end

  def suspended?
    now = Time.zone.now
    has_suspended = suspends_at? && suspends_at <= now
    has_not_unsuspended = unsuspends_at.nil? || unsuspends_at > now
    has_suspended && has_not_unsuspended
  end

  def descriptor
    "#{user ? user.descriptor : '[undefined user]'} is a " \
      "#{role ? role.descriptor : '[undefined role]'}"
  end
end
