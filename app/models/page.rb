require 'filename_with_extension_validator'

# Web pages (html) with metadata.
class Page < ApplicationRecord
  belongs_to :parent, class_name: 'Page', optional: true
  has_many :pages, as: :parent
  has_one :path, as: :item, validate: true, dependent: :destroy

  validates :filename, length: { within: 1..127 }, filename_with_extension: true
  validates :title, presence: true

  before_create :generate_path
  before_update :update_path

  default_scope { includes(:path).order('paths.sitepath') }

  def descriptor
    calculate_sitepath
  end

  protected

  def generate_path
    return self if path
    self.path = Path.new(sitepath: calculate_sitepath)
    self
  end

  def update_path
    path.update!(sitepath: calculate_sitepath)
  end

  def calculate_sitepath
    # append a slash and the filename onto the parent’s (if any) sitepath,
    # and make sure there are no doubled-up slashes
    "#{(parent ? parent.sitepath : '')}/#{filename}".gsub(%r{//+}, '/')
  end

  delegate :sitepath, to: :path
end
