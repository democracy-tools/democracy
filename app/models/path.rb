# Defines custom URLs (“paths”) for arbitrary items on the site,
# or to redirect local URLs to other local paths or to remote URLs.
class Path < ApplicationRecord
  belongs_to :item, polymorphic: true, optional: true

  before_validation :clean_sitepath, if: :sitepath?
  before_validation :clean_redirect, if: :redirect?

  validates(
    :sitepath,
    format: {
      with: %r{\A/(([\w%_~\+\-]+/?)+(\.[\w%_\-]+|/)?)?\z},
      message:
        'must begin with a ‘/’ and be letters, numbers, dashes, percentage signs, ' \
        'underscores and/or slashes, with an optional extension'
    },
    uniqueness: true
  )
  validates(
    :redirect,
    presence: {
      if: proc { |path| (path.item.nil? && path.item_id.nil?) },
      message: 'must have a redirect url/path if not attached to an item on the website'
    },
    format: {
      allow_nil: true,
      with: %r{\A(https?://.+|/(([\w%~_\?=&\-]+/?)+(\.[\w%~_\?=&#\-]+|/)?)?)\z},
      message:
        'must be a valid URL (including ‘http://’) ' \
        'or a valid root-relative sitepath (starts with a slash ‘/’)'
    }
  )

  scope :for_sitepath, ->(searchpath) { where(sitepath: format_path(searchpath)) }

  def self.find_for_sitepath(searchpath)
    for_sitepath(searchpath).first
  end

  # remove any trailing slashes, and ensure leading slash
  def self.format_path(path)
    # strip any trailing slashes
    matches = path.match %r{\A(.*[^/]+)/+\z}
    path = matches[1] if matches
    # ensure leading slash
    path = "/#{path}" unless path[0]&.chr == '/'
    path
  end

  # The sitepath should not end in a slash, except for the root/home path.
  def clean_sitepath
    matches = sitepath.match %r{\A(.*[^/]+)/+\z}
    self.sitepath = matches[1] if matches
    self
  end

  # Clear any leading or trailing whitespace from the redirect.
  def clean_redirect
    self.redirect = redirect.strip unless redirect.nil?
    self
  end

  def descriptor
    sitepath
  end
end
