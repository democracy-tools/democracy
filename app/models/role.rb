require 'filename_validator'

# Defines an access role (authorities) that a user can have.
class Role < ApplicationRecord
  has_many :authorities, dependent: :destroy

  validates :areas, length: { minimum: 1, message: 'must specify at least one area' }
  validates :filename, filename: true
  validates :name, presence: true

  def descriptor
    name
  end
end
