# User Access.
# Managed using the Devise gem http://devise.plataformatec.com.br/
class User < ApplicationRecord
  # Include devise modules. (except for )
  devise(
    :database_authenticatable, # password-based authentication for signing in.
    :registerable, # user registration process, and account edit or destroy.
    # :omniauthable, # OmniAuth (https://github.com/omniauth/omniauth) support.
    :confirmable, # confirmation of email address.
    :lockable, # locks an account after a specified number of failed sign-in attempts.
    # :timeoutable, # expires sessions that have not been active in a specified period of time.
    :recoverable, # resets the user password and sends reset instructions.
    :rememberable, # manages token for remembering the user from a saved cookie.
    :trackable, # tracks sign in count, timestamps and IP address.
    :validatable # provides validations of email and password.
  )

  has_many :authorities, dependent: :destroy

  validates :name, presence: true

  def descriptor
    name
  end
end
