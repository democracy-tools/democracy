require_relative 'html_presenter'

# Meta elements to go in the head element.
class MetadataPresenter < HtmlPresenter
  attr_reader :view, :url, :title, :description, :twitter_creator, :nocache

  # Requires:
  # :view - generally passed in as `self` from a view
  # Accepts:
  # :title
  # :type - defaults to 'article' (or 'website' for root url)
  # :url - defaults to request.url
  # :description
  # :twitter_creator - username of the user responsible for the current page’s content, without '@'
  # :nocache - boolean of whether to flag that the page is not to be remotely cached
  def initialize(params)
    @view = params.fetch(:view)
    @graph_type = params[:type]
    @title = params[:title]
    self.url = params[:url]
    @description = params[:description]
    self.twitter_creator = params[:twitter_creator]
    @nocache = params[:nocache]
  end

  def present_title
    html_tag(:title) { page_title }
  end

  def present_metatags
    present_description +
      present_open_graph_specific +
      present_open_graph_common +
      present_twitter +
      present_cache
  end

  def present_description
    meta_tag(:description, description)
  end

  # The tags that are just used by OpenGraph
  # og:type: “activity”?, Groups: “cause”,
  # Organizations: “government”, “non_profit”, “school”,
  # People: “author”, “politician”, “public_figure”,
  # Places: “City”, “Country”, “state_province”,
  # Websites: “website” site root, “article” site page.
  def present_open_graph_specific
    og_tag(:type, graph_type)
  end

  # The tags that are used by both OpenGraph and Twitter Cards (except image)
  # og:url: canonical url for the content
  # og:title: title for the page/content
  # og:description: max 200 characters
  # og:site_name: name of the website
  def present_open_graph_common
    og_tag(:title, title) +
      og_tag(:url, url) +
      og_tag(:description, description) +
      og_tag(:site_name, site_name)
  end

  # Twitter Card metadata tags
  # twitter:card: summary, photo, gallery, product, app, player
  # twitter:site: the user’s @ username on Twitter
  # twitter:site:id: the user’s id number on Twitter
  # twitter:creator: the content creator’s @ username on Twitter
  # twitter:creator:id: the content creator’s id number on Twitter
  def present_twitter
    twitter_tag(:card, twitter_card_type) +
      twitter_tag(:site, twitter_site) +
      twitter_tag(:creator, twitter_creator)
  end

  def present_cache
    if nocache
      html_tag_with_newline(:meta, name: 'robots', content: 'noindex')
    else
      html_blank
    end
  end

  protected

  def url=(url)
    @url = url
    @url = view.request.url if @url.blank?
  end

  def twitter_creator=(twitter_id)
    @twitter_creator = twitter_id
    needs_at = @twitter_creator.present? && !@twitter_creator[0].eql?('@')
    @twitter_creator = "@#{@twitter_creator}" if needs_at
  end

  def root?
    view.request.path.eql?('/')
  end

  def page_title
    if title.blank? || root?
      site_name
    else
      "#{title} | #{site_name}"
    end
  end

  def graph_type
    @graph_type ||= root? ? 'website' : 'article'
  end

  def site_name
    @site_name ||= Wayground::Metadata::NAME
  end

  def twitter_card_type
    @twitter_card_type ||= 'summary'
  end

  def twitter_site
    @twitter_site ||= "@#{Wayground::Metadata::TWITTER_AT}"
  end

  def meta_tag(key, value)
    meta_tag_or_blank(name: key, content: value)
  end

  def og_tag(key, value)
    meta_tag_or_blank(property: "og:#{key}", content: value)
  end

  def twitter_tag(key, value)
    meta_tag_or_blank(property: "twitter:#{key}", content: value)
  end

  def meta_tag_or_blank(params)
    if params[:content].blank? && params[:value].blank?
      html_blank
    else
      html_tag_with_newline(:meta, params)
    end
  end
end
