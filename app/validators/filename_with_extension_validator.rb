# Validate that a given value is valid as a Wayground-style filename (with no extension).
# Used in ActiveModel/ActiveRecord field validations.
class FilenameWithExtensionValidator < ActiveModel::EachValidator
  DEFAULT_MESSAGE = 'must only be letters, numbers, dashes and underscores; ' \
    'with optional extension; e.g., “A-filename_1.txt”'.freeze

  def validate_each(record, attribute, value)
    valid_filename = value && value.match(%r{\A(/|([\w_~\+\-]+)(\.[\w_]+)?/?)\z})
    record.errors[attribute] << (options[:message] || DEFAULT_MESSAGE) unless valid_filename
  end
end
