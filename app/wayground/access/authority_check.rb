require 'authority'

module Wayground
  module Access

    # Determine the Authority (if any) applicable to a user for a given action & context.
    class AuthorityCheck
      attr_reader :user, :area, :action

      def initialize(user:, area:, action:)
        @user = user
        @area = area
        @action = action
      end

      def authority
        authorities.first
      end

      def authorities
        query.active
      end

      protected

      def query
        Authority
          .where(user_id: user)
          .joins(:role)
          .where('? = ANY (roles.areas)', area)
          .where(roles: { action => true })
      end
    end

  end
end
