module Wayground

  # title: Used in the <title> meta tag. If blank, defaults to the site title.
  # description: Plain text string to be used as the value for the <meta name="description"> tag.
  # nocache: Boolean to instruct browsers and search engines not to cache the content of the page.
  class PageMetadata
    attr_accessor :title, :description, :nocache

    def initialize(title: nil, description: nil, nocache: nil)
      @title = title
      @description = description
      @nocache = nocache
    end

    def merge_params(params)
      @title = params.fetch(:title) if params.include?(:title)
      @description = params.fetch(:description) if params.include?(:description)
      @nocache = params.fetch(:nocache) if params.include?(:nocache)
    end
  end

end
