require 'user'
require 'role'
require 'authority'

module Wayground
  module Setup

    # Handle the initial setup of a fresh installation.
    # Will try to create an initial User with system admin access.
    class Installer
      attr_reader :params, :record_list

      # params should have data for: user record.
      def initialize(params)
        @params = params
      end

      def create_initial_records
        @record_list = build_records
        validate_all
        save_all
        record_list
      end

      protected

      def build_records
        records = {}
        records[:user]        = build_user
        records[:roles]       = build_roles
        records[:authorities] = build_authorities(records.fetch(:user), records.fetch(:roles))
        records
      end

      def build_user
        user_params = params.fetch(:user, {}).permit(
          :name, :email, :password, :password_confirmation
        )
        User.new(user_params)
      end

      def build_roles
        [build_sysadmin_role, build_pageadmin_role]
      end

      def build_sysadmin_role
        Role.new(
          areas: ['system'], filename: 'admin', name: 'System Administrator',
          description: 'Administration and configuration of the system. Manages user access.',
          can_read: true, can_create: true, can_update: true, can_destroy: true,
          can_upload: true, can_comment: true
        )
      end

      def build_pageadmin_role
        Role.new(
          areas: ['page'], filename: 'page_admin', name: 'Pages Editor',
          description: 'Create and manage static pages and custom routes.',
          can_read: true, can_create: true, can_update: true, can_destroy: true,
          can_upload: true, can_comment: true
        )
      end

      # assign all the roles to the user
      def build_authorities(user, roles)
        authorities = []
        roles.each do |role|
          authorities << Authority.new(user: user, role: role, active_at: Time.zone.now)
        end
        authorities
      end

      def validate_all
        record_list.values.each do |record|
          if record.instance_of? Array
            record.each(&:validate!)
          else
            record.validate!
          end
        end
      end

      def save_all
        record_list.values.each do |record|
          if record.instance_of? Array
            record.each(&:save!)
          else
            record.save!
          end
        end
        record_list.fetch(:user).confirm
      end
    end

  end
end
