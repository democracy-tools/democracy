# from http://railscasts.com/episodes/77-destroy-without-javascript-revised
module DeleteResourceRoute
  def resources(*args, &_block)
    super(*args) do
      yield if block_given?
      member do
        get :delete
        delete :delete, action: :destroy
      end
    end
  end
end

ActionDispatch::Routing::Mapper.__send__(:include, DeleteResourceRoute)
