module Wayground
  # attempt to access an area/action the user does not have authority for
  class AccessDenied < RuntimeError; end
  # attempt to access an area/action that requires the user to be signed in
  class LoginRequired < RuntimeError; end
end
