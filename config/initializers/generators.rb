# Use rspec to generate test files.
Rails.application.config.generators do |gen|
  gen.test_framework :rspec
  gen.fixture_replacement :factory_girl, dir: 'spec/factories'
end
