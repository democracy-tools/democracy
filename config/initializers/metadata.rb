module Wayground
  module Metadata
    # Used in page titles
    NAME = 'Calgary Democracy'.freeze
    DESCRIPTION = 'Providing comprehensive, independent, access to online information about ' \
      'our governments and elections affecting Calgarians.'.freeze
    # the Twitter account for the website, without the ‘@’ prefix
    TWITTER_AT = 'yycelect'.freeze
  end
end
