Time::DATE_FORMATS[:db] = '%Y-%m-%d %H:%M:%S'
Time::DATE_FORMATS[:slash_date] = '%Y/%m/%d'
# 3:45:00 PM on Friday, March 1, 2013 :
Time::DATE_FORMATS[:time_date] = '%-l:%M:%S %p on %A, %B %-e, %Y'
Time::DATE_FORMATS[:simple_date] = '%B %-e, %Y' # April 1, 2005
Time::DATE_FORMATS[:flat_date] = '%Y%m%d'
Time::DATE_FORMATS[:form_field_date] = '%B %-e, %Y' # June 2, 2011
# May 5, 2018 at 4:56 PM :
Time::DATE_FORMATS[:form_field_datetime] = '%B %-e, %Y at %-l:%M %p'
Time::DATE_FORMATS[:plain_date] = '%A, %B %-e, %Y' # Friday, March 1, 2013
Time::DATE_FORMATS[:plain_time] = '%-l:%M %p' # 3:45 PM
# Friday, March 1, 2013 at 3:45 PM :
Time::DATE_FORMATS[:plain_datetime] = '%A, %B %-e, %Y at %-l:%M %p'
Time::DATE_FORMATS[:http_header] = '%a, %d %b %Y %H:%M:%S %Z'
Time::DATE_FORMATS[:compact_date] = '%b %-e, %Y' # Oct 2, 2003
# Oct 2, 2003, 5:43:00PM :
Time::DATE_FORMATS[:compact_datetime] = '%b %-e, %Y, %-l:%M:%S%p'
