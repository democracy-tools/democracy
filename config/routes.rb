Rails.application.routes.draw do
  root to: 'routing#root', via: :get

  devise_for :users, skip: [:sessions]
  devise_scope :user do
    # Override paths for login/logout to have simpler paths.
    get 'signin', to: 'devise/sessions#new', as: :new_user_session
    post 'signin', to: 'devise/sessions#create', as: :user_session
    delete 'signout', to: 'devise/sessions#destroy', as: :destroy_user_session
  end

  resources :authorities
  resources :pages
  resources :paths
  resources :roles
  resources :users do
    member do
      get 'unlock', action: :unlock_form
      patch 'unlock', action: :unlock
    end
  end

  get 'about' => 'routing#about'
  get 'fresh_install' => 'routing#fresh_install'
  post 'create_initial_records' => 'routing#create_initial_records'
  get '*url' => 'routing#sitepath', format: false
end
