# Define a path (relative url) to an item.
class CreatePaths < ActiveRecord::Migration[5.1]
  def change
    create_table :paths do |t|
      t.belongs_to :item, polymorphic: true
      t.string :sitepath, null: false
      t.string :redirect
      t.timestamps
    end
    add_index :paths, :sitepath, unique: true
  end
end
