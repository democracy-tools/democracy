# Action roles that can be assigned to users (via authorities).
class CreateRoles < ActiveRecord::Migration[5.1]
  def change
    create_table :roles do |t|
      t.string :areas, null: false, array: true, default: []
      t.string :filename, null: false
      t.string :name, null: false
      t.string :description
      t.boolean :can_read, null: false, default: false
      t.boolean :can_create, null: false, default: false
      t.boolean :can_update, null: false, default: false
      t.boolean :can_destroy, null: false, default: false
      t.boolean :can_upload, null: false, default: false
      t.boolean :can_comment, null: false, default: false

      t.timestamps null: false
    end
    add_index :roles, :areas, using: 'gin'
    add_index :roles, :filename
    add_index :roles, :name
  end
end
