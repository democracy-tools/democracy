# Links a Role to a User, in order to assign access permissions.
class CreateAuthorities < ActiveRecord::Migration[5.1]
  def change
    create_table :authorities do |t|
      t.belongs_to :user, null: false, foreign_key: true
      t.belongs_to :role, null: false, foreign_key: true
      t.datetime :active_at, null: false
      t.datetime :ends_at
      t.datetime :suspends_at
      t.datetime :unsuspends_at
      t.timestamps
    end
    add_index :authorities, %i[user_id role_id], name: 'authority_joins_idx'
    add_index(
      :authorities, %i[active_at ends_at suspends_at unsuspends_at],
      name: 'authority_dates_idx'
    )
  end
end
