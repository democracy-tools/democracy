# An html page.
class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.belongs_to :parent, foreign_key: { to_table: :pages }
      t.string :filename, null: false
      t.string :title, null: false
      t.text :description
      t.text :content
      t.timestamps
    end
  end
end
