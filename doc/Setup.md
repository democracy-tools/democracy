# Setup

TODO: add documentation on how to setup this project both for a development system and as a server.

## Server Configuration

### Environment Variables

The following environment variables will need to be configured for your production server:

* `DEMOCRACY_DATABASE_PASSWORD`: the password for accessing the democracy database in PostgreSQL.
* `DEMOCRACY_DEFAULT_URL_HOST`: the server host domain name (including port number if not the default).
* `DEMOCRACY_DEFAULT_URL_PROTOCOL`: `http` or `https`.
* `SECRET_KEY_BASE`: a string of (typically 128, usually random) characters defining the secret key.
