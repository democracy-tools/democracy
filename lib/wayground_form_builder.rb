# Extensions to the standard form builder.
# Make available by calling `form_for(my_object, builder: WaygroundFormBuilder)`.
class WaygroundFormBuilder < ActionView::Helpers::FormBuilder
  # Displays an array as a series of text fields.
  # Always includes one blank field for adding to the array.
  # TODO: come up with a better interface for dynamically adding-to/removing-from the array
  def array_field(method, options = {})
    options[:multiple] = true
    name = options[:name]
    options[:name] += '[]' if name && !name.match?(/\[\]\z/)
    id_prefix = "#{object.class.name.underscore}_#{method}"
    array_field_fields_for_values(method, options, id_prefix) + \
      text_field(method, options.merge(id: "#{id_prefix}_0", value: nil))
  end

  private

  def array_field_fields_for_values(method, options, id_prefix)
    result = ''.html_safe
    counter = 0
    data_array = object.public_send(method)
    data_array&.each do |value|
      counter += 1
      id = "#{id_prefix}_#{counter}"
      result += text_field(method, options.merge(value: value, id: id)) + "\r\n"
    end
    result
  end
end
