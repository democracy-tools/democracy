require 'rails_helper'
require 'support/shared_examples_for_resource_controllers'
require 'authorities_controller'
require 'authority'
require 'role'
require 'user'

RSpec.describe AuthoritiesController, type: :controller do
  include_context 'user setup', 'system'
  include_examples 'resource controller'

  before(:all) do
    @user = User.order('RANDOM()').first || User.create!(
      username: 'authorityuser', email: 'authority@test.tld'
    )
    @role = Role.order('RANDOM()').first || Role.create!(
      areas: ['system'], filename: 'authority_role', name: 'Authority Role'
    )
  end

  let(:user) { @user }
  let(:role) { @role }

  # Values required for shared examples:

  let(:resource_class) { Authority }

  let(:valid_attributes) do
    $valid_attributes = { user_id: user.id, role_id: role.id, active_at: '2010-11-12 13:14:15' }
  end

  let(:new_attributes) do
    valid_attributes.merge(
      active_at: '2001-01-01 01:01:01',
      ends_at: '2009-09-09 09:09:09',
      suspends_at: '2002-02-02 02:02:02',
      unsuspends_at: '2003-03-03 03:03:03'
    )
  end
  let(:expected_new_values) do
    {
      user: user,
      role: role,
      active_at: Time.zone.parse('2001-01-01 01:01:01'),
      ends_at: Time.zone.parse('2009-09-09 09:09:09'),
      suspends_at: Time.zone.parse('2002-02-02 02:02:02'),
      unsuspends_at: Time.zone.parse('2003-03-03 03:03:03')
    }
  end

  let(:update_attributes) do
    {
      active_at: '2010-10-10 10:10:10',
      ends_at: '2013-03-13 13:13:13',
      suspends_at: '2011-11-11 11:11:11',
      unsuspends_at: '2012-12-12 12:12:12'
    }
  end
  let(:expected_updated_values) do
    {
      active_at: Time.zone.parse('2010-10-10 10:10:10'),
      ends_at: Time.zone.parse('2013-03-13 13:13:13'),
      suspends_at: Time.zone.parse('2011-11-11 11:11:11'),
      unsuspends_at: Time.zone.parse('2012-12-12 12:12:12')
    }
  end

  let(:invalid_attributes) { { user_id: '', role_id: '', active_at: '' } }
  let(:invalid_create_error_symbols) { %i[user role] }

  # /end shared example values

  # Non-standard behaviours for a resource controller:
  describe 'custom behaviours' do
    before(:each) do
      sign_in signin_user
    end
    after(:each) do
      sign_out signin_user
    end

    describe 'GET new' do
      it 'assigns a default time with zone to active at' do
        expect_any_instance_of(Authority).to receive(:active_at=).with(
          instance_of(ActiveSupport::TimeWithZone)
        )
        get :new, params: {}, session: valid_session
      end
      context 'with a user_id' do
        it 'assigns the user_id to the new authority' do
          get :new, params: { user_id: '123' }, session: valid_session
          expect(assigns(:authority).user_id).to eq 123
        end
      end
      context 'with user_id on both the authority and as a simple param' do
        it 'assigns the user_id to the new authority' do
          get(
            :new, params: { authority: { user_id: '123' }, user_id: '456' }, session: valid_session
          )
          expect(assigns(:authority).user_id).to eq 123
        end
      end
      context 'with a role_id' do
        it 'assigns the role_id to the new authority' do
          get :new, params: { role_id: '234' }, session: valid_session
          expect(assigns(:authority).role_id).to eq 234
        end
      end
      context 'with role_id on both the authority and as a simple param' do
        it 'assigns the role_id to the new authority' do
          get(
            :new, params: { authority: { role_id: '345' }, role_id: '456' }, session: valid_session
          )
          expect(assigns(:authority).role_id).to eq 345
        end
      end
    end
  end
end
