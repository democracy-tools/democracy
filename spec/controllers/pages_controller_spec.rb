require 'rails_helper'
require 'support/shared_examples_for_resource_controllers'
require 'pages_controller'
require 'page'

RSpec.describe PagesController, type: :controller do
  include_context 'user setup', 'page'
  include_examples 'resource controller'

  # Values required for shared examples:

  let(:resource_class) { Page }

  let(:valid_attributes) do
    $valid_attributes = { filename: 'valid_page', title: 'Valid Page' }
  end

  let(:new_attributes) { { filename: 'create_page', title: 'Create Page' } }
  let(:expected_new_values) { { filename: 'create_page', title: 'Create Page' } }

  let(:update_attributes) { { filename: 'update_page', title: 'Update Page' } }
  let(:expected_updated_values) { { filename: 'update_page', title: 'Update Page' } }

  let(:invalid_attributes) { { filename: 'not valid', title: '' } }

  # /end shared example values
end
