require 'rails_helper'
require 'support/shared_examples_for_resource_controllers'
require 'paths_controller'
require 'path'

RSpec.describe PathsController, type: :controller do
  include_context 'user setup', 'page'
  include_examples 'resource controller'

  # Values required for shared examples:

  let(:resource_class) { Path }

  let(:valid_attributes) do
    $valid_attributes = { sitepath: '/valid_path', redirect: '/valid_redirect' }
  end

  let(:new_attributes) { { sitepath: '/create_path', redirect: '/create_redirect' } }
  let(:expected_new_values) { { sitepath: '/create_path', redirect: '/create_redirect' } }

  let(:update_attributes) { { sitepath: '/update_path', redirect: '/update_redirect' } }
  let(:expected_updated_values) { { sitepath: '/update_path', redirect: '/update_redirect' } }

  let(:invalid_attributes) { { sitepath: 'not valid', redirect: '' } }

  # /end shared example values
end
