require 'rails_helper'
require 'support/shared_examples_for_resource_controllers'
require 'roles_controller'
require 'role'

RSpec.describe RolesController, type: :controller do
  include_context 'user setup', 'system'
  include_examples 'resource controller'

  # Values required for shared examples:

  let(:resource_class) { Role }

  let(:valid_attributes) do
    $valid_attributes = { areas: ['system'], filename: 'valid', name: 'Valid Role' }
  end

  let(:new_attributes) do
    {
      areas: ['system'],
      filename: 'create_role', name: 'Created Role', description: 'A role via create.',
      can_read: '1', can_create: '0', can_update: '1', can_destroy: '0',
      can_upload: '1', can_comment: '1'
    }
  end
  let(:expected_new_values) do
    {
      areas: ['system'],
      filename: 'create_role',
      name: 'Created Role',
      description: 'A role via create.',
      can_read: true,
      can_create: false,
      can_update: true,
      can_destroy: false,
      can_upload: true,
      can_comment: true
    }
  end

  let(:update_attributes) do
    {
      areas: ['contact', ''], filename: 'edit_role', name: 'Edited Role',
      description: 'A role via edit.',
      can_read: '0', can_create: '1', can_update: '0', can_destroy: '1',
      can_upload: '0', can_comment: '0'
    }
  end
  let(:expected_updated_values) do
    {
      areas: ['contact'],
      filename: 'edit_role',
      name: 'Edited Role',
      description: 'A role via edit.',
      can_read: false,
      can_create: true,
      can_update: false,
      can_destroy: true,
      can_upload: false,
      can_comment: false
    }
  end

  let(:invalid_attributes) { { areas: [], filename: 'not valid', name: '' } }
  let(:invalid_update_error_symbols) { %i[filename name] }

  # /end shared example values
end
