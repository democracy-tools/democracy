require 'rails_helper'

RSpec.describe RoutingController, type: :controller do
  before(:all) do
    Path.delete_all
  end

  context 'with no user records' do
    before(:all) do
      Authority.delete_all
      User.delete_all
    end

    describe 'GET #root' do
      it 'should show the installation form' do
        get :root
        aggregate_failures do
          expect(response).to have_http_status(:success)
          expect(response).to render_template('fresh_install')
        end
      end
    end

    describe 'GET #fresh_install' do
      it 'returns http success' do
        get :fresh_install
        expect(response).to have_http_status(:success)
      end
      it 'should use the about view' do
        get :fresh_install
        expect(response).to render_template('fresh_install')
      end
    end

    describe 'POST #create_initial_records' do
      context 'with valid params' do
        it 'should create the initial records' do
          installer = double(Wayground::Setup::Installer)
          expect(installer).to receive(:create_initial_records).and_return(:installed)
          expect(Wayground::Setup::Installer).to receive(:new).and_return(installer)
          post :create_initial_records, params: { user: {} }
          aggregate_failures do
            expect(assigns(:initial_records)).to eq :installed
            expect(response).to redirect_to(root_url)
            # sets a notice
            expect(request.flash.__send__(:now_is_loaded?)).to be_falsey
            expect(request.flash[:notice]).to match(/installed/i)
            # does not set an alert
            expect(request.flash[:alert]).to be_blank
          end
        end
      end
      context 'with invalid params' do
        it 'should not create the records' do
          post :create_initial_records, params: { user: {}, person: {}, company: {}, business: {} }
          aggregate_failures do
            expect(assigns(:initial_records)).to be_nil
            # returns a success response
            expect(response).to be_success
            # re-renders the 'new' template
            expect(response).to render_template('fresh_install')
            # does not set a notice
            expect(request.flash[:notice]).to be_blank
            # sets an alert that the business failed to be created
            expect(request.flash.__send__(:now_is_loaded?)).to be_truthy
            expect(request.flash.now[:alert]).to match(/invalid/i)
          end
        end
      end
    end
  end

  context 'with user records' do
    before(:all) do
      User.first || create(:confirmed_user)
    end

    describe 'GET #root' do
      context 'with no page' do
        before(:all) do
          Path.where(sitepath: '/').delete_all
          Page.where(filename: '/').delete_all
        end
        it 'returns http success' do
          get :root
          expect(response).to have_http_status(:success)
        end
        it 'should use the root view' do
          get :root
          expect(response).to render_template('root')
        end
      end
      context 'with a custom page' do
        render_views
        before(:all) do
          create(:page, filename: '/', title: 'Home', description: 'Home', content: '<p>home</p>')
        end
        it 'returns http success' do
          get :root
          aggregate_failures do
            expect(response).to have_http_status(:success)
            expect(response).to render_template('routing/page')
            expect(response.body).not_to have_selector('title', text: /home/, visible: false)
            expect(response.body).to have_selector(
              'meta[name="description"][content="Home"]', visible: false
            )
            expect(response.body).to have_selector('p', text: 'home')
          end
        end
      end
    end

    describe 'GET #fresh_install' do
      it 'should deny access' do
        get :fresh_install
        expect(response.status).to eq 403
      end
    end

    describe 'POST #create_initial_records' do
      it 'should deny access' do
        post :create_initial_records, params: { user: {} }
        expect(response.status).to eq 403
      end
    end
  end

  describe 'GET #about' do
    context 'with no page' do
      before(:all) do
        Path.where(sitepath: '/about').delete_all
        Page.where(filename: 'about').delete_all
      end
      it 'returns http success' do
        get :about
        expect(response).to have_http_status(:success)
      end
      it 'should use the about view' do
        get :about
        expect(response).to render_template('about')
      end
    end
    context 'with a custom page' do
      render_views
      before(:all) do
        create(
          :page, filename: 'about', title: 'Custom', description: 'about', content: '<p>custom</p>'
        )
      end
      it 'returns http success' do
        get :about
        aggregate_failures do
          expect(response).to have_http_status(:success)
          expect(response).to render_template('routing/page')
          expect(response.body).to have_selector('title', text: /\ACustom /, visible: false)
          expect(response.body).to have_selector(
            'meta[name="description"][content="about"]', visible: false
          )
          expect(response.body).to have_selector('p', text: 'custom')
        end
      end
    end
  end

  describe 'GET #sitepath' do
    context 'with an unregistered path' do
      it 'shows the item' do
        get :sitepath, params: { url: '/unregistered_url' }
        expect(response).to have_http_status(:missing)
      end
    end
    context 'with an item path' do
      before(:all) do
        create(:item_path, sitepath: '/item_url')
      end
      it 'shows the item' do
        get :sitepath, params: { url: '/item_url' }
        # FIXME: change status code to :success when sitepath show is implemented
        expect(response).to have_http_status(:error)
      end
    end
    context 'with a redirect path' do
      before(:all) do
        create(:redirect_path, sitepath: '/redirect_url', redirect: '/redirect_to')
      end
      it 'shows the item' do
        get :sitepath, params: { url: '/redirect_url' }
        expect(response).to redirect_to('/redirect_to')
      end
    end
    context 'with a custom page' do
      render_views
      before(:all) do
        create(
          :page,
          filename: 'custom', title: 'Custom', description: 'custom', content: '<p>custom</p>'
        )
      end
      it 'returns http success' do
        get :sitepath, params: { url: '/custom' }
        aggregate_failures do
          expect(response).to have_http_status(:success)
          expect(response).to render_template('routing/page')
          expect(response.body).to have_selector('title', text: /\ACustom /, visible: false)
          expect(response.body).to have_selector(
            'meta[name="description"][content="custom"]', visible: false
          )
          expect(response.body).to have_selector('p', text: 'custom')
        end
      end
    end
  end
end
