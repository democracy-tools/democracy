require 'rails_helper'
require 'support/shared_examples_for_resource_controllers'
require 'users_controller'
require 'user'

RSpec.describe UsersController, type: :controller do
  include_context 'user setup', 'system'
  include_examples 'resource controller'

  # Values required for shared examples:

  let(:resource_class) { User }

  let(:valid_attributes) do
    {
      name: 'Valid Name', email: 'valid@email.tld',
      password: 'isvalid', password_confirmation: 'isvalid'
    }
  end

  let(:new_attributes) do
    valid_attributes.merge(
      name: 'New Name', email: 'new@email.tld',
      password: 'newpass', password_confirmation: 'newpass'
    )
  end
  let(:expected_new_values) do
    {
      name: 'New Name', email: 'new@email.tld'
    }
  end

  let(:update_attributes) do
    {
      name: 'Update Name'
    }
  end
  let(:expected_updated_values) do
    {
      name: 'Update Name'
    }
  end

  let(:invalid_attributes) { { name: '', email: 'invalid', password: '' } }
  let(:invalid_update_error_symbols) { %i[name] }

  # /end shared example values

  # Extra controller actions
  describe 'custom behaviours' do
    before(:all) do
      @user = create(:confirmed_user, name: 'Test User', email: 'user@test.tld')
    end
    before(:each) do
      sign_in signin_user
    end
    after(:each) do
      sign_out signin_user
    end

    let(:user) { @user }

    describe 'GET #unlock_form' do
      it 'returns a success response' do
        get :unlock_form, params: { id: user.to_param }, session: valid_session
        expect(response).to be_success
        expect(response).to render_template('unlock_form')
      end
    end

    describe 'PATCH #unlock' do
      it 'removes the lock from the specified user record' do
        user.update(locked_at: 1.hour.ago, failed_attempts: 5)
        patch :unlock, params: { id: user.to_param }, session: valid_session
        expect(assigns(:user).locked_at).to be_nil
        expect(assigns(:user).failed_attempts).to eq 0
        expect(response).to redirect_to(user_url(user))
      end
    end
  end
end
