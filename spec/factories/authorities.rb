FactoryGirl.define do
  factory :authority do
    user
    role
    active_at '2017-01-01 00:00:00'
    # ends_at '2001-01-01 00:00:00'
    # suspends_at '2001-01-01 00:00:00'
    # unsuspends_at '2001-01-01 00:00:00'
  end
end
