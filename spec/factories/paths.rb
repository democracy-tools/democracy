FactoryGirl.define do
  factory :path, aliases: [:redirect_path] do
    # don’t use a raw :path factory — only use a sub-factory
    sequence(:sitepath) { |n| "/factory/path_#{n}_#{rand(100)}" }
    redirect '/'
    factory :item_path do
      association :item, factory: :redirect_path
      redirect nil
    end
  end
end
