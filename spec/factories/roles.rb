FactoryGirl.define do
  factory :role do
    areas ['system']
    sequence(:filename) { |n| "factory_role_#{n}" }
    name 'Factory Role'
    description 'Factory generated role.'
    can_read false
    can_create false
    can_update false
    can_destroy false
    can_upload false
    can_comment false
    factory :role_with_full_access do
      name 'Factory Full Access Role'
      can_read true
      can_create true
      can_update true
      can_destroy true
      can_upload true
      can_comment true
    end
  end
end
