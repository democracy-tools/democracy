FactoryGirl.define do
  factory :user do
    sequence(:name) { |n| "Factory User #{n}" }
    sequence(:email) { |n| "factoryuser_#{n}_#{rand(100)}@test.tld" }
    password 'password'
    factory :confirmed_user do
      confirmed_at '2001-01-01 00:00:00 UTC'
    end
  end
end
