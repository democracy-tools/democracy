require 'rails_helper'

# validate the generated html
require 'html_validation'
include PageValidations
HaveValidHTML.show_html_in_failures = true

def extract_from_email_sent(pattern)
  email = ActionMailer::Base.deliveries.last
  message = email.body.to_s
  matches = message.match(pattern)
  matches[1]
end

# Make sure we’re starting fresh for user signins
Authority.delete_all
User.delete_all
Role.delete_all
