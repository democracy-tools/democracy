require 'feature_helper'
require 'support/shared_examples_for_resource_features'
require 'authority'

RSpec.feature 'AuthoritiesFeatures', type: :feature do
  include_context 'feature user setup', 'system'
  include_examples 'resource feature', Authority

  before(:all) do
    @user = create(:confirmed_user)
    @role = create(:role, name: 'Authority Feature Role')
  end
  after(:all) do
    @user&.destroy
    @role&.destroy
  end

  let(:user) { @user }
  let(:role) { @role }

  # parameters for shared examples
  let(:new_attributes) do
    {
      'User' => user.id.to_s,
      'authority_role_id' => { method: :select, value: role.descriptor },
      'Active at' => '2001-01-01 01:01',
      'Ends at' => '2004-04-04 04:04',
      'Suspends at' => '2002-02-02 02:02',
      'Unsuspends at' => '2003-03-03 03:03'
    }
  end
  let(:expected_new_values) do
    [
      # heading
      {
        selector: "h2 > a[href='/users/#{user.to_param}'][class='icon user']",
        text: user.descriptor
      },
      {
        selector: "h2 > a[href='/roles/#{role.to_param}'][class='icon role']",
        text: role.descriptor
      },
      # table listing fields
      { selector: 'td[class="datetime"]', text: '2001-01-01 01:01:00 UTC' },
      { selector: 'td[class="datetime"]', text: '2004-04-04 04:04:00 UTC' },
      { selector: 'td[class="datetime"]', text: '2002-02-02 02:02:00 UTC' },
      { selector: 'td[class="datetime"]', text: '2003-03-03 03:03:00 UTC' }
    ]
  end

  let(:update_attributes) do
    {
      'User' => user.id.to_s,
      'authority_role_id' => { method: :select, value: role.descriptor },
      'Active at' => '2005-05-05 05:05',
      'Ends at' => '2008-08-08 08:08',
      'Suspends at' => '2006-06-06 06:06',
      'Unsuspends at' => '2007-07-07 07:07'
    }
  end
  let(:expected_updated_values) do
    [
      # heading
      {
        selector: "h2 > a[href='/users/#{user.to_param}'][class='icon user']",
        text: user.descriptor
      },
      {
        selector: "h2 > a[href='/roles/#{role.to_param}'][class='icon role']",
        text: role.descriptor
      },
      # table listing fields
      { selector: 'td[class="datetime"]', text: '2005-05-05 05:05:00 UTC' },
      { selector: 'td[class="datetime"]', text: '2008-08-08 08:08:00 UTC' },
      { selector: 'td[class="datetime"]', text: '2006-06-06 06:06:00 UTC' },
      { selector: 'td[class="datetime"]', text: '2007-07-07 07:07:00 UTC' }
    ]
  end

  let(:invalid_attributes) do
    {
      'User' => '', 'authority_role_id' => { method: :select, value: '' },
      'Active at' => '',
      'Suspends at' => '2009-09-09 09:09:09', 'Unsuspends at' => '2009-09-09 09:09:08'
    }
  end
  let(:invalid_attribute_keys) { %i[user role unsuspends_at] } # active_at

  let(:descriptor_method) { :id }
end
