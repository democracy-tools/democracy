require 'feature_helper'
require 'support/shared_examples_for_resource_features'
require 'user'

RSpec.feature 'DeviseFeatures', type: :feature do
  include_context 'feature user setup', 'system'

  scenario 'New user registration, confirmation, and sign in' do
    # sign up as a new user
    visit '/users/sign_up'
    expect(page).to have_valid_html
    fill_in 'Name', with: 'Devise Feature'
    fill_in 'Email', with: 'sign_up@devise.feature.tld'
    fill_in 'Password', with: 'new_user'
    fill_in 'Password confirmation', with: 'new_user'
    click_button 'Sign up'
    expect(page).to have_valid_html
    expect(page).to have_selector('#flash .notice', text: /email.+activate/i)
    # not signed in
    expect(page).not_to have_selector('nav > ul > li#usermenu')

    # try to confirm with invalid token
    visit '/users/confirmation?confirmation_token=invalid'
    expect(page).to have_valid_html
    expect(page).to have_selector('#error_explanation li', text: 'Confirmation token is invalid')

    # request that the confirmation instructions be sent again
    visit '/signin'
    click_on "Didn't receive confirmation instructions?"
    fill_in 'Email', with: 'sign_up@devise.feature.tld'
    click_button 'Resend confirmation instructions'
    expect(page).to have_selector('#flash .notice', text: /email.+confirm/i)

    # confirm it
    confirmation_link = extract_from_email_sent(
      %r{href="[^"]+(/users/confirmation\?confirmation_token=[^"]+)">}
    )
    visit confirmation_link
    expect(page).to have_valid_html
    expect(page).to have_selector('#flash .notice', text: /email.+confirmed/i)
    # not signed in
    expect(page).not_to have_selector('nav > ul > li#usermenu')

    # sign in with newly confirmed user
    visit '/signin'
    expect(page).to have_valid_html
    fill_in 'Email', with: 'sign_up@devise.feature.tld'
    fill_in 'Password', with: 'new_user'
    click_button 'Sign in'
    expect(page).to have_valid_html
    expect(page).to have_selector('#flash .notice', text: /signed.*in/i)
    # when signed in, the usermenu should be present:
    expect(page).to have_selector('nav > ul > li#usermenu')
  end

  # FIXME: get session quitting (with cookie retention) working to test remember signin
  # scenario 'Remember signin across sessions' do
  #   visit '/signin'
  #   fill_in 'Email', with: 'signin@feature.tld'
  #   fill_in 'Password', with: 'password'
  #   check 'Remember me'
  #   click_button 'Sign in'
  #   # quit session <- this is where things aren’t working. any suggestions?
  #   Capybara.reset_sessions!
  #   visit '/'
  #   # should be signed in
  #   expect(page).to have_selector('nav > ul > li#usermenu')
  # end

  scenario 'forgot password' do
    # get and fill out the forgot password form
    visit '/signin'
    click_on 'Forgot your password?'
    expect(page).to have_valid_html
    fill_in 'Email', with: 'signin@feature.tld'
    click_button 'Send me reset password instructions'
    expect(page).to have_selector('#flash .notice', text: /password recovery.+email/i)
    # go to the recovery link from the email
    recovery_link = extract_from_email_sent(
      %r{href="[^"]+(/users/password/edit\?reset_password_token=[^"]+)">}
    )
    visit recovery_link
    expect(page).to have_valid_html
    # fill in the change password form
    fill_in 'New password', with: 'password'
    fill_in 'Confirm new password', with: 'password'
    click_button 'Change my password'
    expect(page).to have_selector('#flash .notice', text: /password.+changed/i)
    # sign in with the changed password
    fill_in 'Email', with: 'signin@feature.tld'
    fill_in 'Password', with: 'password'
    click_button 'Sign in'
    expect(page).to have_selector('#flash .notice', text: /signed.*in/i)
    expect(page).to have_selector('nav > ul > li#usermenu')
  end

  scenario 'Sign out' do
    signin_with_user
    # TODO: if testing with javascript
    # page.accept_alert 'Are you sure you want to sign out?' do
    #   click_on 'Sign out'
    # end
    click_on 'Sign out'
    expect(page).to have_valid_html
    expect(page).to have_selector('#flash .notice', text: /signed.*out/i)
    # no user menu when signed out
    expect(page).not_to have_selector('nav > ul > li#usermenu')
  end

  scenario 'user gets locked and unlocked' do
    # try to sign in with bad password 5 times
    visit '/signin'
    fill_in 'Email', with: 'signin@feature.tld'
    fill_in 'Password', with: 'badpassword'
    click_button 'Sign in'
    expect(page).to have_selector('#flash .alert', text: /invalid.+email.+password/i)
    expect(page).not_to have_selector('nav > ul > li#usermenu')
    fill_in 'Email', with: 'signin@feature.tld'
    fill_in 'Password', with: 'anotherbadpassword'
    click_button 'Sign in'
    fill_in 'Email', with: 'signin@feature.tld'
    fill_in 'Password', with: 'badpassword3'
    click_button 'Sign in'
    fill_in 'Email', with: 'signin@feature.tld'
    fill_in 'Password', with: 'fourthbadpassword'
    click_button 'Sign in'
    fill_in 'Email', with: 'signin@feature.tld'
    fill_in 'Password', with: 'badpassword5'
    click_button 'Sign in'
    # should now be locked
    user = User.where(email: 'signin@feature.tld').first
    expect(user.failed_attempts).to eq 5
    expect(user.locked_at).to be_present

    # try to unlock with invalid token
    visit '/users/unlock?unlock_token=invalid'
    expect(page).to have_valid_html
    expect(page).to have_selector('#error_explanation li', text: 'Unlock token is invalid')

    # request that the unlock instructions be sent again
    visit '/signin'
    click_on "Didn't receive unlock instructions?"
    fill_in 'Email', with: 'sign_up@devise.feature.tld'
    click_button 'Resend unlock instructions'
    expect(page).to have_selector('#flash .notice', text: /email.+unlock/i)

    # get the unlock link from the email sent
    unlock_link = extract_from_email_sent(
      %r{href="[^"]+(/users/unlock\?unlock_token=[^"]+)">Unlock my account}
    )
    expect(unlock_link).to be_present
    # unlock
    visit unlock_link
    expect(page).to have_valid_html
    expect(page).to have_selector('#flash .notice', text: /account.+unlocked/i)
    expect(page).not_to have_selector('nav > ul > li#usermenu')

    # should be showing the signin form
    # sign in correctly
    fill_in 'Email', with: 'signin@feature.tld'
    fill_in 'Password', with: 'password'
    click_button 'Sign in'
    expect(page).to have_selector('#flash .notice', text: /signed.*in/i)
    expect(page).to have_selector('nav > ul > li#usermenu')
  end
end
