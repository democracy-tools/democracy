require 'feature_helper'
require 'support/shared_examples_for_resource_features'
require 'page'

# putting database clearing at the top here,
# so it doesn’t collide with records created in the included examples
Page.delete_all

RSpec.feature 'PagesFeatures', type: :feature do
  include_context 'feature user setup', 'page'
  include_examples 'resource feature', Page

  # parameters for shared examples
  let(:new_attributes) do
    {
      'Filename' => 'new_page', 'Title' => 'New Feature Page',
      'Description' => 'Page created by feature.', 'Content' => '<h1>Page by Feature</h1>'
    }
  end
  let(:expected_new_values) do
    [
      { selector: 'td > code', text: 'new_page' },
      { selector: 'td', text: 'New Feature Page' },
      { selector: 'td', text: 'Page created by feature.' },
      { selector: 'td', text: '<h1>Page by Feature</h1>' }
    ]
  end

  let(:update_attributes) do
    {
      'Filename' => 'updated_page', 'Title' => 'Updated Feature Page',
      'Description' => 'Page updated by feature.', 'Content' => '<p>Feature updated.</p>'
    }
  end
  let(:expected_updated_values) do
    [
      { selector: 'td > code', text: 'updated_page' },
      { selector: 'td', text: 'Updated Feature Page' },
      { selector: 'td', text: 'Page updated by feature.' },
      { selector: 'td', text: '<p>Feature updated.</p>' }
    ]
  end

  let(:invalid_attributes) { { 'Filename' => 'invalid path', 'Title' => '' } }
  let(:invalid_attribute_keys) { %i[filename title] }
end
