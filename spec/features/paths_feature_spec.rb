require 'feature_helper'
require 'support/shared_examples_for_resource_features'
require 'path'

# putting database clearing at the top here,
# so it doesn’t collide with records created in the included examples
Path.delete_all

RSpec.feature 'PathsFeatures', type: :feature do
  include_context 'feature user setup', 'page'
  include_examples 'resource feature', Path

  # parameters for shared examples
  let(:new_attributes) { { 'Sitepath' => '/featurepath', 'Redirect' => '/destination/path' } }
  let(:expected_new_values) do
    [
      { selector: 'td > code', text: '/featurepath' },
      { selector: 'td > code', text: '/destination/path' }
    ]
  end

  let(:update_attributes) do
    { 'Sitepath' => '/updatedpath', 'Redirect' => '/update/redirect/path' }
  end
  let(:expected_updated_values) do
    [
      { selector: 'td > code', text: '/updatedpath' },
      { selector: 'td > code', text: '/update/redirect/path' }
    ]
  end

  let(:invalid_attributes) { { 'Sitepath' => 'invalid path', 'Redirect' => '' } }
  let(:invalid_attribute_keys) { %i[sitepath redirect] }
end
