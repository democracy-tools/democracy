require 'feature_helper'
require 'support/shared_examples_for_resource_features'
require 'path'

RSpec.feature 'RolesFeatures', type: :feature do
  include_context 'feature user setup', 'system'
  include_examples 'resource feature', Role

  # parameters for shared examples
  let(:new_attributes) do
    {
      'Areas' => 'page',
      'Filename' => 'new_role',
      'Name' => 'New Role',
      'Description' => 'Description for new role.',
      'Can read' => { method: :check },
      'Can create' => { method: :check },
      'Can update' => { method: :check },
      'Can destroy' => { method: :check },
      'Can upload' => { method: :check },
      'Can comment' => { method: :check }
    }
  end
  let(:expected_new_values) do
    [
      { selector: 'h1', text: 'New Role' },
      { selector: 'p', text: 'Description for new role.' },
      { selector: 'li', text: 'page' },
      { selector: 'code', text: 'new_role' },
      { selector: 'td[class="icon read true"]', text: 'Read' },
      { selector: 'td[class="icon create true"]', text: 'Create' },
      { selector: 'td[class="icon update true"]', text: 'Update' },
      { selector: 'td[class="icon destroy true"]', text: 'Destroy' },
      { selector: 'td[class="icon upload true"]', text: 'Upload' },
      { selector: 'td[class="icon comment true"]', text: 'Comment' }
    ]
  end

  let(:update_attributes) do
    {
      'Areas' => 'contact',
      'Filename' => 'edited_role',
      'Name' => 'Edited Role',
      'Description' => 'Description for edited role.',
      'Can read' => { method: :uncheck },
      'Can create' => { method: :uncheck },
      'Can update' => { method: :uncheck },
      'Can destroy' => { method: :uncheck },
      'Can upload' => { method: :uncheck },
      'Can comment' => { method: :uncheck }
    }
  end
  let(:expected_updated_values) do
    [
      { selector: 'h1', text: 'Edited Role' },
      { selector: 'p', text: 'Description for edited role.' },
      { selector: 'li', text: 'system' },
      { selector: 'li', text: 'contact' },
      { selector: 'code', text: 'edited_role' },
      { selector: 'td[class="icon read false"]', text: 'Read' },
      { selector: 'td[class="icon create false"]', text: 'Create' },
      { selector: 'td[class="icon update false"]', text: 'Update' },
      { selector: 'td[class="icon destroy false"]', text: 'Destroy' },
      { selector: 'td[class="icon upload false"]', text: 'Upload' },
      { selector: 'td[class="icon comment false"]', text: 'Comment' }
    ]
  end

  let(:invalid_attributes) do
    {
      'Areas' => '',
      'Filename' => '',
      'Name' => ''
    }
  end
  let(:invalid_edit_attributes) do
    {
      # clear the existing area:
      'role[areas][]' => { method: :fill_in, params: { with: '', id: 'role_areas_1' } },
      'Filename' => '',
      'Name' => ''
    }
  end
  let(:invalid_attribute_keys) { %i[areas filename name] }
end
