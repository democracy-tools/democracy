require 'feature_helper'
require 'support/shared_examples_for_resource_features'
require 'path'

RSpec.feature 'RoutingFeature', type: :feature do
  context 'with an existing user' do
    before(:all) do
      User.first || create(:confirmed_user)
    end

    context 'with no paths' do
      before(:all) do
        Path.delete_all
      end

      scenario 'home page' do
        visit '/'
        expect(page).to have_valid_html
        expect(page).to have_selector('h1', text: 'New Site Installation')
      end

      scenario 'about page' do
        visit '/about'
        expect(page).to have_valid_html
        expect(page).to have_selector('h1', text: 'About')
      end

      scenario 'unrecognized path' do
        visit '/unrecognized-path'
        expect(page).to have_valid_html
        expect(page).to have_selector('#flash .alert', text: /not found/i)
        expect(page).to have_selector('h1', text: '404 Not Found')
      end
    end

    context 'with custom redirect paths' do
      before(:all) do
        Path.delete_all
        create(:redirect_path, sitepath: '/', redirect: '/signin')
        create(:redirect_path, sitepath: '/about', redirect: '/signin')
        create(:redirect_path, sitepath: '/feature/custom/path', redirect: '/signin')
      end

      scenario 'home page' do
        visit '/'
        expect(page).to have_valid_html
        expect(page).to have_selector('h2', text: 'Log in')
      end

      scenario 'about page' do
        visit '/about'
        expect(page).to have_valid_html
        expect(page).to have_selector('h2', text: 'Log in')
      end

      scenario 'custom redirect path' do
        visit '/feature/custom/path'
        expect(page).to have_valid_html
        expect(page).to have_selector('h2', text: 'Log in')
      end
    end

    context 'with custom pages' do
      before(:all) do
        Path.delete_all
        Page.delete_all
        create(:page, filename: '/', title: 'Home', description: 'Home', content: '<p>home</p>')
        create(
          :page, filename: 'about', title: 'About', description: 'About', content: '<p>about</p>'
        )
        create(
          :page,
          filename: 'custom_page', title: 'Custom', description: 'Custom', content: '<p>custom</p>'
        )
      end

      scenario 'home page' do
        visit '/'
        expect(page).to have_valid_html
        expect(page).to have_selector('p', text: 'home')
      end

      scenario 'about page' do
        visit '/about'
        expect(page).to have_valid_html
        expect(page).to have_selector('p', text: 'about')
      end

      scenario 'custom page' do
        visit '/custom_page'
        expect(page).to have_valid_html
        expect(page).to have_selector('p', text: 'custom')
      end
    end
  end

  context 'with no user' do
    before(:all) do
      Path.delete_all
      Authority.delete_all
      User.delete_all
    end

    scenario 'installation' do
      visit '/'
      expect(page).to have_valid_html
      fill_in 'Name', with: 'Install Admin'
      fill_in 'Email', with: 'user@install.feature.tld'
      fill_in 'Password', with: 'install_user'
      fill_in 'Password confirmation', with: 'install_user'
      click_button 'Setup Installation'
      expect(page).to have_valid_html
      expect(page).to have_selector('#flash .notice', text: /installed/i)
      # not signed in
      expect(page).not_to have_selector('nav > ul > li#usermenu')

      # sign in as the new admin user
      visit '/signin'
      fill_in 'Email', with: 'user@install.feature.tld'
      fill_in 'Password', with: 'install_user'
      click_button 'Sign in'
      # check that can access system actions
      click_on 'Roles'
      expect(page).to have_selector('th > a', text: 'System Admin')
    end
  end
end
