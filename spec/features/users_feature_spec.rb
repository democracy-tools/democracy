require 'feature_helper'
require 'support/shared_examples_for_resource_features'
require 'user'

RSpec.feature 'UsersFeatures', type: :feature do
  include_context 'feature user setup', 'system'
  include_examples 'resource feature', User

  # parameters for shared examples
  let(:omit_feature_actions) { { new: true } }

  let(:update_attributes) { { 'Name' => 'Update Feature' } }
  let(:expected_updated_values) { [{ selector: 'p .name', text: 'Update Feature' }] }

  let(:invalid_attributes) do
    { 'Name' => '', 'Email' => 'bad email', 'Password' => '', 'Password confirmation' => 'invalid' }
  end
  let(:invalid_edit_attributes) { { 'Name' => '' } }
  let(:invalid_attribute_keys) { %i[name email password] }
  let(:invalid_edit_attribute_keys) { %i[name] }

  # /end shared example values

  # Extra features
  context 'non-standard features' do
    before(:all) do
      @unconfirmed = create(:user, name: 'One', email: 'one@extra.tld')
      @confirmed = create(:confirmed_user, name: 'Two', email: 'two@extra.tld')
      @signed_in = create(
        :confirmed_user,
        name: 'Three', email: 'three@extra.tld',
        sign_in_count: '123', last_sign_in_at: '2012-03-04 05:06:07 UTC'
      )
      @lock_time = Time.zone.now
      @locked = create(
        :confirmed_user,
        name: 'Four', email: 'four@extra.tld', locked_at: @lock_time, failed_attempts: '345'
      )
    end
    before(:each) do
      signin_with_user
    end

    scenario 'Index' do
      visit '/users'
      expect(page).to have_valid_html
      aggregate_failures do
        # unconfirmed user
        id = @unconfirmed.id
        expect(page).to have_selector("tr#user_#{id} > td > a[href='/users/#{id}']", text: 'One')
        expect(page).to have_selector("tr#user_#{id} > td[class='email']", text: 'one@extra.tld')
        expect(page).to have_selector("tr#user_#{id} > td[class='false']", text: '—')
        expect(page).to have_selector("tr#user_#{id} > td[class='icon unlock']", text: '')
        # confirmed user
        id = @confirmed.id
        expect(page).to have_selector("tr#user_#{id} > td > a[href='/users/#{id}']", text: 'Two')
        expect(page).to have_selector("tr#user_#{id} > td[class='email']", text: 'two@extra.tld')
        expect(page).to have_selector(
          "tr#user_#{id} > td[class='true datetime']", text: '2001-01-01 00:00:00 UTC'
        )
        expect(page).to have_selector("tr#user_#{id} > td[class='icon unlock']", text: '')
        # signed in user
        id = @signed_in.id
        expect(page).to have_selector("tr#user_#{id} > td > a[href='/users/#{id}']", text: 'Three')
        expect(page).to have_selector("tr#user_#{id} > td[class='email']", text: 'three@extra.tld')
        expect(page).to have_selector(
          "tr#user_#{id} > td[class='true datetime']", text: '2001-01-01 00:00:00 UTC'
        )
        expect(page).to have_selector("tr#user_#{id} > td[class='numeric']", text: '123')
        expect(page).to have_selector(
          "tr#user_#{id} > td[class='datetime']", text: '2012-03-04 05:06:07 UTC'
        )
        expect(page).to have_selector("tr#user_#{id} > td[class='icon unlock']", text: '')
        # locked user
        id = @locked.id
        expect(page).to have_selector("tr#user_#{id} > td > a[href='/users/#{id}']", text: 'Four')
        expect(page).to have_selector("tr#user_#{id} > td[class='email']", text: 'four@extra.tld')
        expect(page).to have_selector(
          "tr#user_#{id} > td[class='true datetime']", text: '2001-01-01 00:00:00 UTC'
        )
        expect(page).to have_selector(
          "tr#user_#{id} > td[class='icon lock datetime']", text: @lock_time.to_s
        )
        expect(page).to have_selector(
          "tr#user_#{id} > td > a[href='/users/#{id}/unlock']", text: 'Unlock'
        )
      end
    end
  end
end
