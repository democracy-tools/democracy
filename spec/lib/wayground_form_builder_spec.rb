require 'rails_helper'
require 'wayground_form_builder'

# mock class to substitute for ActiveRecord models that will be used in actual usage
class TestItem
  include ActiveModel::Model
  attr_accessor :names
end

def item(params = {})
  TestItem.new(params)
end

RSpec.describe WaygroundFormBuilder, type: :view do
  describe '#array_field' do
    let :template do
      <<-EOTEMPLATE
      <%= form_for(item, builder: WaygroundFormBuilder, url: 'testitem') do |f| %>
      <%= f.array_field(:names) %>
      <% end %>
      EOTEMPLATE
    end
    it 'should generate a text field' do
      render inline: template, locals: { item: item }
      expect(rendered).to have_selector('input[type="text"]')
    end
    it 'should assign the right field name' do
      render inline: template, locals: { item: item }
      expect(rendered).to have_selector('input[name="test_item[names][]"]')
    end
    it 'should set the values' do
      render inline: template, locals: { item: item(names: %w[abc def]) }
      expect(rendered).to have_selector('input[value="abc"]')
      expect(rendered).to have_selector('input[value="def"]')
    end
    it 'should generate multiple fields' do
      render inline: template, locals: { item: item(names: %w[ghi jkl]) }
      expect(rendered).to have_selector(
        'input[id="test_item_names_1"] + input[id="test_item_names_2"] + ' \
        'input[id="test_item_names_0"]'
      )
    end
    context 'with no array value on the item' do
      it 'should not have a value' do
        render inline: template, locals: { item: item(names: nil) }
        expect(rendered).not_to have_selector('input[value]')
      end
    end
    context 'with an empty array value on the item' do
      it 'should not have a value' do
        render inline: template, locals: { item: item(names: []) }
        # because there is a value on names (even though it’s empty)
        # a (empty) value attribute gets added to the tag
        expect(rendered).not_to have_selector('input[value]')
      end
    end
    context 'with multiple items in the array value on the item' do
      it 'should have white-space between the input fields' do
        render inline: template, locals: { item: item(names: %w[a b c]) }
        # because there is a value on names (even though it’s empty)
        # a (empty) value attribute gets added to the tag
        expect(rendered).to match(
          /
            <input[^>]+name="test_item\[names\]\[\]"[^>]*>\s+
            <input[^>]+name="test_item\[names\]\[\]"
          /x
        )
      end
    end
    context 'with options' do
      let :template do
        <<-EOTEMPLATE
        <%= form_for(item, builder: WaygroundFormBuilder, url: 'testitem') do |f| %>
        <%= f.array_field(:names, title: 'title-test') %>
        <% end %>
        EOTEMPLATE
      end
      it 'should render the extra attributes' do
        render inline: template, locals: { item: item(names: %w[a b]) }
        expect(rendered).to have_selector(
          'input[name="test_item[names][]"][title="title-test"][value="a"]'
        )
      end
    end
    context 'with an array name option' do
      let :template do
        <<-EOTEMPLATE
        <%= form_for(item, builder: WaygroundFormBuilder, url: 'testitem') do |f| %>
        <%= f.array_field(:names, name: 'override[]') %>
        <% end %>
        EOTEMPLATE
      end
      it 'should set the name based on the options' do
        render inline: template, locals: { item: item(names: %w[c d]) }
        expect(rendered).to have_selector('input[name="override[]"][value="c"]')
      end
    end
    context 'with a non-array name option' do
      let :template do
        <<-EOTEMPLATE
        <%= form_for(item, builder: WaygroundFormBuilder, url: 'testitem') do |f| %>
        <%= f.array_field(:names, name: 'notarray') %>
        <% end %>
        EOTEMPLATE
      end
      it 'should change the name into array format' do
        render inline: template, locals: { item: item(names: %w[e f]) }
        expect(rendered).to have_selector('input[name="notarray[]"][value="e"]')
      end
    end
  end
end
