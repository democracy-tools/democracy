require 'rails_helper'
require 'authority'

RSpec.describe Authority, type: :model do
  before(:all) do
    Authority.delete_all
    @user = create(:confirmed_user, name: 'Authority User')
    @role = create(:role, areas: ['system'], filename: 'authority_role', name: 'Authority Role')
  end
  let(:user) { @user }
  let(:role) { @role }
  let(:min_param) { { user: user, role: role, active_at: '2001-02-03 04:05:06' } }
  let(:default_err_msg) { 'is invalid' }

  def pretend_time_is(time)
    allow(Time.zone).to receive(:now).and_return(Time.zone.parse(time))
  end

  # Validations

  describe 'general validation' do
    it 'should pass with the minimum params' do
      authority = Authority.new(min_param)
      expect(authority.valid?).to be_truthy
    end
  end

  describe '#user validation' do
    it 'should fail if user is nil' do
      authority = Authority.new(min_param.merge(user: nil))
      expect(authority.valid?).to be_falsey
      err_msg = authority.errors[:user][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
  end

  describe '#role validation' do
    it 'should fail if role is nil' do
      authority = Authority.new(min_param.merge(role: nil))
      expect(authority.valid?).to be_falsey
      err_msg = authority.errors[:role][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
  end

  describe '#active_at validation' do
    it 'should fail if active_at is nil' do
      authority = Authority.new(min_param.merge(active_at: nil))
      expect(authority.valid?).to be_falsey
      err_msg = authority.errors[:active_at][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
    it 'should fail if active_at is blank' do
      authority = Authority.new(min_param.merge(active_at: ''))
      expect(authority.valid?).to be_falsey
      err_msg = authority.errors[:active_at][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
  end

  describe '#suspends_at validation' do
    it 'should pass if suspends_at is nil' do
      authority = Authority.new(
        min_param.merge(active_at: '2001-01-01 01:01:01', suspends_at: nil)
      )
      expect(authority.valid?).to be_truthy
    end
    it 'should fail if suspends_at is less than active_at' do
      authority = Authority.new(
        min_param.merge(active_at: '2002-02-02 02:02:02', suspends_at: '2002-02-02 02:02:01')
      )
      expect(authority.valid?).to be_falsey
      err_msg = authority.errors[:suspends_at][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
    it 'should pass if suspends_at is the same as active_at' do
      authority = Authority.new(
        min_param.merge(active_at: '2003-03-03 03:03:03', suspends_at: '2003-03-03 03:03:03')
      )
      expect(authority.valid?).to be_truthy
    end
    it 'should pass if suspends_at is after active_at' do
      authority = Authority.new(
        min_param.merge(active_at: '2004-04-04 04:04:04', suspends_at: '2004-04-04 04:04:05')
      )
      expect(authority.valid?).to be_truthy
    end
  end

  describe '#ends_at validation' do
    it 'should fail if ends_at is less than active_at' do
      authority = Authority.new(
        min_param.merge(active_at: '2001-01-01 01:01:01', ends_at: '2001-01-01 01:01:00')
      )
      expect(authority.valid?).to be_falsey
      err_msg = authority.errors[:ends_at][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
    it 'should fail if ends_at is the same as active_at' do
      authority = Authority.new(
        min_param.merge(active_at: '2002-02-02 02:02:02', ends_at: '2002-02-02 02:02:02')
      )
      expect(authority.valid?).to be_falsey
      err_msg = authority.errors[:ends_at][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
    it 'should pass if ends_at is right after active_at' do
      authority = Authority.new(
        min_param.merge(active_at: '2003-03-03 03:03:03', ends_at: '2003-03-03 03:03:04')
      )
      expect(authority.valid?).to be_truthy
    end
    it 'should fail, but not set an error on ends_at, if ends_at is present without active_at' do
      authority = Authority.new(
        min_param.merge(active_at: nil, ends_at: '2004-04-04 04:04:04')
      )
      expect(authority.valid?).to be_falsey
      expect(authority.errors[:ends_at]).to eq []
    end
    it 'should pass, and not set an error on ends_at, if ends_at is absent' do
      authority = Authority.new(
        min_param.merge(active_at: '2005-05-05 05:05:05', ends_at: nil)
      )
      expect(authority.valid?).to be_truthy
      expect(authority.errors[:ends_at]).to eq []
    end
    it 'should fail, but not set an error on ends_at, if ends_at and active_at are both absent' do
      authority = Authority.new(
        min_param.merge(active_at: nil, ends_at: nil)
      )
      expect(authority.valid?).to be_falsey
      expect(authority.errors[:ends_at]).to eq []
    end
  end

  describe '#unsuspends_at validation' do
    it 'should fail if unsuspends_at is less than suspends_at' do
      authority = Authority.new(
        min_param.merge(suspends_at: '2001-01-01 01:01:01', unsuspends_at: '2001-01-01 01:01:00')
      )
      expect(authority.valid?).to be_falsey
      err_msg = authority.errors[:unsuspends_at][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
    it 'should fail if unsuspends_at is the same as suspends_at' do
      authority = Authority.new(
        min_param.merge(suspends_at: '2002-02-02 02:02:02', unsuspends_at: '2002-02-02 02:02:02')
      )
      expect(authority.valid?).to be_falsey
      err_msg = authority.errors[:unsuspends_at][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
    it 'should pass if unsuspends_at is right after suspends_at' do
      authority = Authority.new(
        min_param.merge(suspends_at: '2003-03-03 03:03:03', unsuspends_at: '2003-03-03 03:03:04')
      )
      expect(authority.valid?).to be_truthy
    end
    it 'should pass, with no error on unsuspends_at, if just unsuspends_at is present' do
      authority = Authority.new(
        min_param.merge(suspends_at: nil, unsuspends_at: '2004-04-04 04:04:04')
      )
      expect(authority.valid?).to be_truthy
      expect(authority.errors[:unsuspends_at]).to eq []
    end
    it 'should pass, with no error on unsuspends_at, if unsuspends_at is absent' do
      authority = Authority.new(
        min_param.merge(suspends_at: '2005-05-05 05:05:05', unsuspends_at: nil)
      )
      expect(authority.valid?).to be_truthy
      expect(authority.errors[:unsuspends_at]).to eq []
    end
    it 'should pass, with no error on unsuspends_at, if unsuspends_at and suspends_at are absent' do
      authority = Authority.new(
        min_param.merge(suspends_at: nil, unsuspends_at: nil)
      )
      expect(authority.valid?).to be_truthy
      expect(authority.errors[:unsuspends_at]).to eq []
    end
  end

  # Scopes

  describe 'Scopes' do
    before(:all) do
      @authorities = {}
      @authorities[:active] = create(
        :authority, user: @user, role: @role, active_at: '2001-01-01 01:01:01'
      )
      @authorities[:active_ends] = create(
        :authority,
        user: @user, role: @role, active_at: '2001-01-01 01:01:02', ends_at: '2010-10-10 10:10:10'
      )
      @authorities[:active_suspended] = create(
        :authority,
        user: @user, role: @role, active_at: '2001-01-01 01:01:03',
        suspends_at: '2010-10-10 10:10:10'
      )
      @authorities[:active_unsuspended] = create(
        :authority,
        user: @user, role: @role, active_at: '2001-01-01 01:01:04',
        suspends_at: '2002-02-02 02:02:02', unsuspends_at: '2010-10-10 10:10:10'
      )
      @authorities[:active_unsuspended_and_ends] = create(
        :authority,
        user: @user, role: @role, active_at: '2001-01-01 01:01:05', ends_at: '2010-10-10 10:10:10',
        suspends_at: '2002-02-02 02:02:02', unsuspends_at: '2003-03-03 03:03:03'
      )
    end
    let(:authorities) { @authorities }

    before(:each) { pretend_time_is('2010-10-10 10:10:10') }

    describe '#active' do
      it 'should find the ones that haven’t ended and are not suspended' do
        found = Authority.active.order(:active_at).to_a
        expect(found).to eq [authorities[:active], authorities[:active_unsuspended]]
      end
    end

    describe '#expired' do
      it 'should find the ones that have ended' do
        # limit to the authorities we created for this test
        found = Authority.where(
          'active_at >= ? AND active_at <= ?', '2001-01-01 01:01:01', '2001-01-01 01:01:05'
        )
        found = found.expired.order(:active_at).to_a
        expect(found).to eq [authorities[:active_ends], authorities[:active_unsuspended_and_ends]]
      end
    end

    describe '#suspended' do
      it 'should find the ones that are currently suspended but have not ended' do
        found = Authority.suspended.order(:active_at).to_a
        expect(found).to eq [authorities[:active_suspended]]
      end
    end
  end

  # Methods

  describe '#active?' do
    before(:each) { pretend_time_is('2001-01-01 01:01:01') }

    context 'with active_at in the past' do
      it 'should return true' do
        authority = Authority.new(active_at: '2001-01-01 01:01:00')
        expect(authority.active?).to be_truthy
      end
    end
    context 'with active_at right now' do
      it 'should return true' do
        authority = Authority.new(active_at: '2001-01-01 01:01:01')
        expect(authority.active?).to be_truthy
      end
    end
    context 'with active_at in the future' do
      it 'should return false' do
        authority = Authority.new(active_at: '2001-01-01 01:01:02')
        expect(authority.active?).to be_falsey
      end
    end
    context 'with ends_at in the past' do
      it 'should return false' do
        authority = Authority.new(active_at: '2001-01-01 00:00:00', ends_at: '2001-01-01 01:01:00')
        expect(authority.active?).to be_falsey
      end
    end
    context 'with ends_at right now' do
      it 'should return false' do
        authority = Authority.new(active_at: '2001-01-01 00:00:00', ends_at: '2001-01-01 01:01:01')
        expect(authority.active?).to be_falsey
      end
    end
    context 'with ends_at in the future' do
      it 'should return true' do
        authority = Authority.new(active_at: '2001-01-01 00:00:00', ends_at: '2001-01-01 01:01:02')
        expect(authority.active?).to be_truthy
      end
    end
    context 'with suspension started in past' do
      it 'should return false' do
        authority = Authority.new(
          active_at: '2001-01-01 00:00:00', suspends_at: '2001-01-01 01:01:00'
        )
        expect(authority.active?).to be_falsey
      end
    end
    context 'with suspension starting now' do
      it 'should return false' do
        authority = Authority.new(
          active_at: '2001-01-01 00:00:00', suspends_at: '2001-01-01 01:01:01'
        )
        expect(authority.active?).to be_falsey
      end
    end
    context 'with suspension starting in the future' do
      it 'should return true' do
        authority = Authority.new(
          active_at: '2001-01-01 00:00:00', suspends_at: '2001-01-01 01:01:02'
        )
        expect(authority.active?).to be_truthy
      end
    end
    context 'with suspension ending in past' do
      it 'should return true' do
        authority = Authority.new(
          active_at: '2001-01-01 00:00:00', suspends_at: '2001-01-01 01:00:00',
          unsuspends_at: '2001-01-01 01:01:00'
        )
        expect(authority.active?).to be_truthy
      end
    end
    context 'with suspension ending now' do
      it 'should return true' do
        authority = Authority.new(
          active_at: '2001-01-01 00:00:00', suspends_at: '2001-01-01 01:00:00',
          unsuspends_at: '2001-01-01 01:01:01'
        )
        expect(authority.active?).to be_truthy
      end
    end
    context 'with suspension ending in the future' do
      it 'should return false' do
        authority = Authority.new(
          active_at: '2001-01-01 00:00:00', suspends_at: '2001-01-01 01:00:00',
          unsuspends_at: '2001-01-01 01:01:02'
        )
        expect(authority.active?).to be_falsey
      end
    end
  end

  describe '#suspended?' do
    before(:each) { pretend_time_is('2001-01-01 01:01:01') }

    context 'with suspension is nil' do
      it 'should return false' do
        authority = Authority.new(active_at: '2001-01-01 00:00:00', suspends_at: nil)
        expect(authority.suspended?).to be_falsey
      end
    end
    context 'with suspension started in past' do
      it 'should return true' do
        authority = Authority.new(
          active_at: '2001-01-01 00:00:00', suspends_at: '2001-01-01 01:01:00'
        )
        expect(authority.suspended?).to be_truthy
      end
    end
    context 'with suspension starting now' do
      it 'should return true' do
        authority = Authority.new(
          active_at: '2001-01-01 00:00:00', suspends_at: '2001-01-01 01:01:01'
        )
        expect(authority.suspended?).to be_truthy
      end
    end
    context 'with suspension starting in the future' do
      it 'should return false' do
        authority = Authority.new(
          active_at: '2001-01-01 00:00:00', suspends_at: '2001-01-01 01:01:02'
        )
        expect(authority.suspended?).to be_falsey
      end
    end
    context 'with suspension ending in past' do
      it 'should return false' do
        authority = Authority.new(
          active_at: '2001-01-01 00:00:00', suspends_at: '2001-01-01 01:00:00',
          unsuspends_at: '2001-01-01 01:01:00'
        )
        expect(authority.suspended?).to be_falsey
      end
    end
    context 'with suspension ending now' do
      it 'should return false' do
        authority = Authority.new(
          active_at: '2001-01-01 00:00:00', suspends_at: '2001-01-01 01:00:00',
          unsuspends_at: '2001-01-01 01:01:01'
        )
        expect(authority.suspended?).to be_falsey
      end
    end
    context 'with suspension ending in the future' do
      it 'should return true' do
        authority = Authority.new(
          active_at: '2001-01-01 00:00:00', suspends_at: '2001-01-01 01:00:00',
          unsuspends_at: '2001-01-01 01:01:02'
        )
        expect(authority.suspended?).to be_truthy
      end
    end
    context 'with suspension starting and ending in the future' do
      it 'should return false' do
        authority = Authority.new(
          active_at: '2001-01-01 00:00:00', suspends_at: '2001-01-01 01:01:02',
          unsuspends_at: '2001-01-01 01:01:03'
        )
        expect(authority.suspended?).to be_falsey
      end
    end
  end

  describe '#descriptor' do
    context 'without user or role' do
      it 'should have no identified relations' do
        authority = Authority.new
        expect(authority.descriptor).to eq '[undefined user] is a [undefined role]'
      end
    end
    context 'with just a user' do
      it 'should identify the user' do
        authority = Authority.new(user: user)
        expect(authority.descriptor).to eq 'Authority User is a [undefined role]'
      end
    end
    context 'with just a role' do
      it 'should identify the role' do
        authority = Authority.new(role: role)
        expect(authority.descriptor).to eq '[undefined user] is a Authority Role'
      end
    end
    context 'with a user and role' do
      it 'should identify the user and role' do
        authority = Authority.new(user: user, role: role)
        expect(authority.descriptor).to eq 'Authority User is a Authority Role'
      end
    end
  end
end
