require 'rails_helper'

RSpec.describe Page, type: :model do
  let(:filename_param) { 'valid' }
  let(:title_param) { 'Valid Test' }
  let(:minimum_params) { { filename: filename_param, title: title_param } }

  describe 'validation' do
    let(:page) { Page.new(minimum_params) }

    context 'with the minimum params' do
      it 'should pass' do
        expect(page.valid?).to be_truthy
      end
    end

    context 'with a nil filename' do
      let(:filename_param) { nil }
      it 'should fail' do
        expect(page.valid?).to be_falsey
        expect(page.errors[:filename][0]).to match(/too short/i)
      end
    end
    context 'with a blank filename' do
      let(:filename_param) { '' }
      it 'should fail' do
        expect(page.valid?).to be_falsey
        expect(page.errors[:filename][0]).to match(/too short/i)
      end
    end
    context 'with a single slash filename' do
      let(:filename_param) { '/' }
      it 'should pass' do
        expect(page.valid?).to be_truthy
      end
    end
    context 'with a slash and characters in the filename' do
      let(:filename_param) { '/filename' }
      it 'should fail' do
        expect(page.valid?).to be_falsey
        expect(page.errors[:filename][0]).to match(/must/i)
      end
    end
    context 'with a leading period in the filename' do
      let(:filename_param) { '.filename' }
      it 'should fail' do
        expect(page.valid?).to be_falsey
        expect(page.errors[:filename][0]).to match(/must/i)
      end
    end
    context 'with a trailing period in the filename' do
      let(:filename_param) { 'filename.' }
      it 'should fail' do
        expect(page.valid?).to be_falsey
        expect(page.errors[:filename][0]).to match(/must/i)
      end
    end
    context 'with a consecutive periods in the filename' do
      let(:filename_param) { 'file..name' }
      it 'should fail' do
        expect(page.valid?).to be_falsey
        expect(page.errors[:filename][0]).to match(/must/i)
      end
    end
    context 'with a high-byte character in the filename' do
      let(:filename_param) { 'ƒilename' }
      it 'should fail' do
        expect(page.valid?).to be_falsey
        expect(page.errors[:filename][0]).to match(/must/i)
      end
    end
    context 'with an ampersand in the filename' do
      let(:filename_param) { 'file&name' }
      it 'should fail' do
        expect(page.valid?).to be_falsey
        expect(page.errors[:filename][0]).to match(/must/i)
      end
    end
    context 'with 1 character in the filename' do
      let(:filename_param) { 'a' }
      it 'should pass' do
        expect(page.valid?).to be_truthy
      end
    end
    context 'with 127 characters in the filename' do
      let(:filename_param) { 'a' * 127 }
      it 'should pass' do
        expect(page.valid?).to be_truthy
      end
    end
    context 'with 128 characters in the filename' do
      let(:filename_param) { 'a' * 128 }
      it 'should fail' do
        expect(page.valid?).to be_falsey
        expect(page.errors[:filename][0]).to match(/too long/i)
      end
    end
    context 'with all the allowed characters in the filename' do
      let(:filename_param) { 'ABCDEFGHIJKLMNOPQRSTUVWXYZ-abcdefghijklmnopqrstuvwxyz_01234567.89' }
      it 'should pass' do
        expect(page.valid?).to be_truthy
      end
    end

    context 'with a nil title' do
      let(:title_param) { nil }
      it 'should fail' do
        expect(page.valid?).to be_falsey
        expect(page.errors[:title][0]).to match(/blank/i)
      end
    end
    context 'with a blank title' do
      let(:title_param) { '' }
      it 'should fail' do
        expect(page.valid?).to be_falsey
        expect(page.errors[:title][0]).to match(/blank/i)
      end
    end
  end

  describe '#generate_path' do
    it 'should be called after the Page is saved' do
      page = Page.new(filename: 'generate_path', title: 'Generate Path')
      page.save!
      expect(page.path).to be_a Path
    end
  end

  describe '#update_path' do
    it 'should make no change to the path if the Page’s filename did not change' do
      page = create(:page, filename: 'original')
      page.update!(description: 'Not changing the filename.')
      expect(page.sitepath).to eq '/original'
    end
    it 'should update the path if the Page’s filename changed' do
      page = create(:page, filename: 'original')
      page.update!(filename: 'changed')
      expect(page.sitepath).to eq '/changed'
    end
  end

  describe '#calculate_sitepath' do
    it 'should just be the filename with a leading slash if no parent Page' do
      page = Page.new(filename: 'page', title: 'Page')
      expect(page.__send__(:calculate_sitepath)).to eq '/page'
    end
    it 'should have be the parent’s sitepath plus a slash and the filename' do
      parent = build(:page, filename: 'parent')
      parent.__send__(:generate_path)
      page = Page.new(parent: parent, filename: 'page', title: 'Page')
      expect(page.__send__(:calculate_sitepath)).to eq '/parent/page'
    end
    it 'should just be a slash for the home Page' do
      page = Page.new(filename: '/', title: 'Home')
      expect(page.__send__(:calculate_sitepath)).to eq '/'
    end
  end

  describe '#sitepath' do
    it 'should be the path’s sitepath' do
      page = build(:page, filename: 'testpage')
      page.__send__(:generate_path)
      expect(page.sitepath).to eq '/testpage'
    end
  end

  describe '#descriptor' do
    it 'should alias the sitepath' do
      page = build(:page, filename: 'testpage')
      page.__send__(:generate_path)
      expect(page.descriptor).to eq '/testpage'
    end
  end
end
