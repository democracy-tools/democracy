require 'rails_helper'

RSpec.describe Path, type: :model do
  before(:all) do
    Path.delete_all
    @path = create(:redirect_path, sitepath: '/spec_sitepath')
  end

  let(:sitepath) { '/test' }
  let(:redirect) { '/' }
  let(:minimum_params) { { sitepath: sitepath, redirect: redirect } }
  let(:path) { Path.new(minimum_params) }

  describe 'validations' do
    context 'with the minimum params' do
      it 'should fail' do
        expect(path.valid?).to be_truthy
      end
    end

    context 'with a nil sitepath' do
      let(:sitepath) { nil }
      it 'should fail' do
        expect(path.valid?).to be_falsey
        expect(path.errors[:sitepath][0]).to match(/must/i)
      end
    end
    context 'with an empty sitepath' do
      let(:sitepath) { '' }
      it 'should fail' do
        expect(path.valid?).to be_falsey
        expect(path.errors[:sitepath][0]).to match(/must/i)
      end
    end
    context 'with multiple periods in sitepath' do
      let(:sitepath) { '/file.name.etc' }
      it 'should fail' do
        expect(path.valid?).to be_falsey
        expect(path.errors[:sitepath][0]).to match(/must/i)
      end
    end
    context 'with spaces in sitepath' do
      let(:sitepath) { '/file name' }
      it 'should fail' do
        expect(path.valid?).to be_falsey
        expect(path.errors[:sitepath][0]).to match(/must/i)
      end
    end
    context 'with a single slash in sitepath' do
      let(:sitepath) { '/' }
      it 'should pass' do
        expect(path.valid?).to be_truthy
      end
    end
    context 'with letters, numbers, dash, percentage sign, underscore and slash in sitepath' do
      let(:sitepath) { '/ABCEDFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/0123456789-%_' }
      it 'should pass' do
        expect(path.valid?).to be_truthy
      end
    end
    context 'with a file extension in sitepath' do
      let(:sitepath) { '/file.extension' }
      it 'should pass' do
        expect(path.valid?).to be_truthy
      end
    end
    context 'with a duplicate sitepath' do
      let(:sitepath) { '/spec_sitepath' } # already exists
      it 'should fail' do
        expect(path.valid?).to be_falsey
        expect(path.errors[:sitepath][0]).to match(/taken/i)
      end
    end

    context 'with a nil redirect' do
      let(:redirect) { nil }
      it 'should fail' do
        expect(path.valid?).to be_falsey
        expect(path.errors[:redirect][0]).to match(/must.+redirect/i)
      end
    end
    context 'with a blank redirect' do
      let(:redirect) { '' }
      it 'should fail' do
        expect(path.valid?).to be_falsey
        expect(path.errors[:redirect][0]).to match(/must.+redirect/i)
      end
    end
    context 'with an http url redirect' do
      let(:redirect) { 'http://host.tld/' }
      it 'should fail' do
        expect(path.valid?).to be_truthy
      end
    end
    context 'with an https url redirect' do
      let(:redirect) { 'https://host.tld/' }
      it 'should fail' do
        expect(path.valid?).to be_truthy
      end
    end
    context 'with an unrecognized url type redirect' do
      let(:redirect) { 'ftp://host.tld/' }
      it 'should fail' do
        expect(path.valid?).to be_falsey
        expect(path.errors[:redirect][0]).to match(/must.+valid URL/i)
      end
    end
    context 'with a root-relative path redirect' do
      let(:redirect) { '/redirect' }
      it 'should fail' do
        expect(path.valid?).to be_truthy
      end
    end
  end

  describe 'scopes' do
    describe '.for_sitepath' do
      it 'should return an empty array when no matching Path' do
        expect(Path.for_sitepath('/non-existant').to_a).to eq []
      end
      it 'should return the Path when the sitepath matches' do
        expect(Path.for_sitepath('/spec_sitepath').to_a).to eq [@path]
      end
      it 'should return the Path when the sitepath to search for has an extra trailing slash' do
        expect(Path.for_sitepath('/spec_sitepath/').to_a).to eq [@path]
      end
      it 'should return the Path when the sitepath to search for has no leading slash' do
        expect(Path.for_sitepath('spec_sitepath').to_a).to eq [@path]
      end
    end
  end

  # METHODS

  describe '.find_for_sitepath' do
    context 'with a non-existant sitepath' do
      it 'should return nil' do
        expect(Path.find_for_sitepath('/non-existant')).to be_nil
      end
    end
    context 'with a sitepath with leading slash' do
      it 'should return the Path' do
        expect(Path.find_for_sitepath('/spec_sitepath')).to eq @path
      end
    end
    context 'with a sitepath with no leading slash' do
      it 'should return the Path' do
        expect(Path.find_for_sitepath('spec_sitepath')).to eq @path
      end
    end
  end

  describe '.format_path' do
    context 'given a blank path' do
      it 'should return the root path' do
        expect(Path.format_path('')).to eq '/'
      end
    end
    context 'given a slash path' do
      it 'should return the root path' do
        expect(Path.format_path('/')).to eq '/'
      end
    end
    context 'given a path with no slashes' do
      it 'should return prepend a slash' do
        expect(Path.format_path('a_path_with_no.slashes')).to eq '/a_path_with_no.slashes'
      end
    end
    context 'given a path with a leading slash' do
      it 'should return the path' do
        expect(Path.format_path('/leading.slash')).to eq '/leading.slash'
      end
    end
    context 'given a path with a trailing slash' do
      it 'should return the path without the trailing slash' do
        expect(Path.format_path('/trailing.slash/')).to eq '/trailing.slash'
      end
    end
    context 'given a path without a leading slash and with a trailing slash' do
      it 'should return the path with a leading slash and without the trailing slash' do
        expect(Path.format_path('no_lead.but_trail/')).to eq '/no_lead.but_trail'
      end
    end
  end

  describe '#clean_sitepath' do
    it 'should ignore a blank sitepath' do
      path = Path.new(sitepath: '')
      expect(path.clean_sitepath.sitepath).to eq ''
    end
    it 'should ignore the root sitepath' do
      path = Path.new(sitepath: '/')
      expect(path.clean_sitepath.sitepath).to eq '/'
    end
    it 'should leave a sitepath with no trailing slash alone' do
      path = Path.new(sitepath: '/no-trailing')
      expect(path.clean_sitepath.sitepath).to eq '/no-trailing'
    end
    it 'should strip a trailing slash from sitepath' do
      path = Path.new(sitepath: '/trailing/')
      expect(path.clean_sitepath.sitepath).to eq '/trailing'
    end
    it 'should strip multiple trailing slashes from sitepath' do
      path = Path.new(sitepath: '/excess////')
      expect(path.clean_sitepath.sitepath).to eq '/excess'
    end
    it 'should filter the sitepath before validating the Path' do
      path = Path.new(sitepath: '/trailing/')
      path.valid?
      expect(path.sitepath).to eq '/trailing'
    end
    it 'should return the path object (self)' do
      path = Path.new(sitepath: 'test')
      expect(path.clean_sitepath).to be path
    end
  end

  describe '#clean_redirect' do
    context 'with a nil redirect' do
      it 'should leave the redirect as nil' do
        path = Path.new(redirect: nil)
        expect(path.clean_redirect.redirect).to eq nil
      end
    end
    context 'with a blank redirect' do
      it 'should leave the redirect blank' do
        path = Path.new(redirect: '')
        expect(path.clean_redirect.redirect).to eq ''
      end
    end
    context 'with a single space in redirect' do
      it 'should make the redirect blank' do
        path = Path.new(redirect: ' ')
        expect(path.clean_redirect.redirect).to eq ''
      end
    end
    context 'with a bunch of leading and trailing white-space in redirect' do
      it 'should make the redirect just the text within the white-space' do
        path = Path.new(redirect: " \r\n\t lots\r\n\r\n\t  ")
        expect(path.clean_redirect.redirect).to eq 'lots'
      end
    end
    it 'should return the path object (self)' do
      path = Path.new
      expect(path.clean_redirect).to be path
    end
  end

  describe '#descriptor' do
    it 'should just alias sitepath' do
      path = Path.new(sitepath: '/describe/Path')
      expect(path.descriptor).to eq '/describe/Path'
    end
  end
end
