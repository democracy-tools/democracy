require 'rails_helper'

RSpec.describe Role, type: :model do
  let(:min_param) { $min_param = { areas: ['system'], filename: 'minimum', name: 'Minimum' } }
  let(:default_err_msg) { 'is invalid' }

  # Relations

  describe 'Relations' do
    describe '#authorities' do
      it 'should allow assignment of an authority' do
        role = Role.new
        authority = Authority.new
        role.authorities << authority
        expect(role.authorities.size).to eq 1
        expect(role.authorities.first).to eq authority
      end
    end
  end

  # Validations

  describe '#areas validation' do
    it 'should fail if areas is blank' do
      role = Role.new(min_param.merge(areas: ''))
      expect(role.valid?).to be_falsey
      err_msg = role.errors[:areas][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
    it 'should fail if areas is an empty array' do
      role = Role.new(min_param.merge(areas: []))
      expect(role.valid?).to be_falsey
      err_msg = role.errors[:areas][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
  end

  describe '#filename validation' do
    it 'should fail if filename is blank' do
      role = Role.new(min_param.merge(filename: ''))
      expect(role.valid?).to be_falsey
      err_msg = role.errors[:filename][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
    it 'should fail if filename is nil' do
      role = Role.new(min_param)
      role.filename = nil
      expect(role.valid?).to be_falsey
      err_msg = role.errors[:filename][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
    it 'should pass if filename is one character' do
      role = Role.new(min_param.merge(filename: '1'))
      expect(role.valid?).to be_truthy
      expect(role.errors[:filename]).to eq []
    end
    it 'should pass if filename is all it’s allowed characters' do
      role = Role.new(min_param.merge(filename: '0123456789-abcdefghijklmnopqrstuvwxyz_'))
      expect(role.valid?).to be_truthy
      expect(role.errors[:filename]).to eq []
    end
    it 'should fail if filename starts with a dash' do
      role = Role.new(min_param.merge(filename: '-'))
      expect(role.valid?).to be_falsey
      err_msg = role.errors[:filename][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
    it 'should fail if filename contains an upper-case letter' do
      role = Role.new(min_param.merge(filename: 'A'))
      expect(role.valid?).to be_falsey
      err_msg = role.errors[:filename][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
    it 'should fail if filename contains a space' do
      role = Role.new(min_param.merge(filename: 'a b'))
      expect(role.valid?).to be_falsey
      err_msg = role.errors[:filename][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
    it 'should fail if filename contains a percent symbol' do
      role = Role.new(min_param.merge(filename: 'a%20b'))
      expect(role.valid?).to be_falsey
      err_msg = role.errors[:filename][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
  end

  describe '#name validation' do
    it 'should fail if name is blank' do
      role = Role.new(min_param.merge(name: ''))
      expect(role.valid?).to be_falsey
      err_msg = role.errors[:name][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
    it 'should fail if name is nil' do
      role = Role.new(min_param)
      role.name = nil
      expect(role.valid?).to be_falsey
      err_msg = role.errors[:name][0]
      expect(err_msg).to be_a String
      expect(err_msg).not_to eq default_err_msg
    end
    it 'should pass if name is one character' do
      role = Role.new(min_param.merge(name: '1'))
      expect(role.valid?).to be_truthy
      expect(role.errors[:name]).to eq []
    end
  end

  describe '#descriptor' do
    it 'should just be an alias to abbrev' do
      role = Role.new(name: 'Descriptor')
      expect(role.descriptor).to eq 'Descriptor'
    end
  end
end
