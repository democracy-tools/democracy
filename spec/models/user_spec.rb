require 'rails_helper'
require 'user'

RSpec.describe User, type: :model do
  let(:name) { 'minimum' }
  let(:minimum_params) do
    $minimum_params = { name: name, email: 'user@model.tld', password: 'password' }
  end

  describe 'name validations' do
    let(:user) { $user = User.new(minimum_params) }
    context 'with a nil value' do
      let(:name) { nil }
      it 'should fail' do
        expect(user).to be_invalid
      end
    end
    context 'with an empty value' do
      let(:name) { '' }
      it 'should fail' do
        expect(user).to be_invalid
      end
    end
    context 'with a valid character value' do
      let(:name) { 'a' }
      it 'should pass' do
        expect(user).to be_valid
      end
    end
  end

  describe '#descriptor' do
    it 'should just alias name' do
      user = User.new(name: 'Describe User')
      expect(user.descriptor).to eq 'Describe User'
    end
  end
end
