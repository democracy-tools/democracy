require 'rails_helper'

RSpec.describe AuthoritiesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/authorities').to route_to('authorities#index')
    end
    it 'routes to #show' do
      expect(get: '/authorities/1').to route_to('authorities#show', id: '1')
    end

    it 'routes to #new' do
      expect(get: '/authorities/new').to route_to('authorities#new')
    end
    it 'routes to #create' do
      expect(post: '/authorities').to route_to('authorities#create')
    end

    it 'routes to #edit' do
      expect(get: '/authorities/1/edit').to route_to('authorities#edit', id: '1')
    end
    it 'routes to #update via PUT' do
      expect(put: '/authorities/1').to route_to('authorities#update', id: '1')
    end
    it 'routes to #update via PATCH' do
      expect(patch: '/authorities/1').to route_to('authorities#update', id: '1')
    end

    it 'routes to #delete' do
      expect(get: '/authorities/1/delete').to route_to('authorities#delete', id: '1')
    end
    it 'routes to #destroy' do
      expect(delete: '/authorities/1').to route_to('authorities#destroy', id: '1')
    end
  end
end
