require 'rails_helper'

RSpec.describe RoutingController, type: :routing do
  describe 'routing' do
    it 'handles the root route' do
      expect(get: '/').to route_to(controller: 'routing', action: 'root')
    end
    it 'handles the about route' do
      expect(get: '/about').to route_to(controller: 'routing', action: 'about')
    end
    it 'handles arbitrary, otherwise unrecognized, routes' do
      expect(get: '/arbitrary/route.whatever').to route_to(
        controller: 'routing', action: 'sitepath', url: 'arbitrary/route.whatever'
      )
    end
    it 'handles the fresh_install route' do
      expect(get: '/fresh_install').to route_to(controller: 'routing', action: 'fresh_install')
    end
    it 'handles the create_initial_records route' do
      expect(post: '/create_initial_records').to route_to(
        controller: 'routing', action: 'create_initial_records'
      )
    end
  end
end
