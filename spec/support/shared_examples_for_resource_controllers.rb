require 'rails_helper'

# The following must be defined using “let(:something)” syntax
# in the specs that are referencing these shared examples:
# REQUIRED:
# - resource_class: Class
# - valid_attributes: hash
# - new_attributes: hash; can be same as valid_attributes, but should fill in all, not just required
# - expected_new_values: hash (keys are resource field names, values are the values)
# - update_attributes: hash (different values than valid_attributes)
# - expected_updated_values: hash (keys are resource field names, values are the values)
# - invalid_attributes: hash
# OPTIONAL:
# - invalid_create_error_symbols: array of symbols for the fields made invalid by invalid_attributes
# - invalid_update_error_symbols: array of symbols for the fields made invalid by invalid_attributes

RSpec.shared_context 'user setup' do |resource_area|
  before(:all) do
    Authority.delete_all
    User.delete_all
    @signin_user = create(:confirmed_user)
    @signin_role = create(:role_with_full_access, areas: [resource_area], name: 'Signin Role')
    @signin_authority = create(:authority, user: @signin_user, role: @signin_role)
  end
  let(:signin_user) { @signin_user }
end

RSpec.shared_context 'common values for resource controller' do
  let(:valid_session) { {} }

  let(:resource) { resource_class.create!(valid_attributes) }
  let(:resource_symbol) { resource_class.name.underscore.to_sym }
  let(:resources_symbol) { resource_class.name.underscore.pluralize.to_sym }

  let(:invalid_create_error_symbols) { invalid_attributes.keys }
  let(:invalid_update_error_symbols) { invalid_create_error_symbols }
end

# REQUIRED:
# - search_method: symbol; the method/scope on the resource class used to search
RSpec.shared_examples 'a searchable index' do
  context 'signed in' do
    before(:each) do
      sign_in signin_user
    end
    after(:each) do
      sign_out signin_user
    end

    describe 'GET searchable index' do
      context 'with a search term' do
        it 'assigns @resources based on the search term' do
          expect(resource_class).to receive(search_method).with('search').and_return(:searched)
          get :index, params: { term: 'search' }, session: valid_session
          expect(assigns(resources_symbol)).to eq :searched
        end
      end
      context 'with a search blank term' do
        it 'assigns @resources ignoring the search term' do
          expect(resource_class).to receive(:all).and_return(:ignored_search_term)
          get :index, params: { term: '' }, session: valid_session
          expect(assigns(resources_symbol)).to eq :ignored_search_term
        end
      end
    end
  end
end

# This example is split out on its own because there can be a number of different
# sets of invalid params to be tested, depending on the model for the controller.
RSpec.shared_examples 'an invalid update' do
  context 'signed in' do
    before(:each) do
      sign_in signin_user
    end
    after(:each) do
      sign_out signin_user
    end

    describe 'PUT invalid update' do
      context 'with invalid params' do
        it 'does not update' do
          put(
            :update,
            params: { id: resource.to_param, resource_symbol => invalid_attributes },
            session: valid_session
          )
          resource = assigns(resource_symbol)
          aggregate_failures do
            expect(resource).to be_a(resource_class)
            expect(response).to be_success
            # re-renders the 'edit' template
            expect(response).to render_template('edit')
            # sets a notice that the business failed to be updated
            expect(request.flash.__send__(:now_is_loaded?)).to be_truthy
            expect(request.flash.now[:alert]).to match(/could not/i)
            # assigns the errors for the invalid fields
            errors = resource.errors
            invalid_update_error_symbols.each do |error_field|
              expect(errors[error_field].first).to be_present
            end
          end
        end
      end
    end
  end
end

RSpec.shared_examples 'resource controller' do
  include_context 'common values for resource controller'

  context 'signed in' do
    before(:each) do
      sign_in signin_user
    end
    after(:each) do
      sign_out signin_user
    end

    let(:resources_url) { public_send "#{resource_symbol.to_s.pluralize}_url" }
    let(:bad_resource_id) { (resource_class.select(:id).last&.id).to_i + 100_000 }

    describe 'GET index' do
      before(:each) do
        unless [Authority, User].include?(resource_class)
          @resources = build_stubbed_list(resource_symbol, 3)
          allow(resource_class).to receive(:all).and_return(@resources)
        end
      end
      it 'returns a success response' do
        get :index, params: {}, session: valid_session
        expect(response).to be_success
      end
      it 'assigns all resources as to a variable' do
        if [Authority, User].include?(resource_class)
          expected = resource_class.all
        else
          expected = :all_resources
          allow(resource_class).to receive(:all).and_return(expected)
        end
        get :index, params: {}, session: valid_session
        expect(assigns(resources_symbol)).to eq expected
      end
      it 'assigns the right number of resources' do
        get :index, params: {}, session: valid_session
        resources_count = resource_class.count
        expect(resources_count).to(satisfy { |count| count > 0 })
        expect(assigns(resources_symbol).count).to eq resources_count
      end
    end

    describe 'GET show' do
      it 'returns a success response' do
        allow(resource_class).to receive(:find).with('234').and_return(resource)
        get :show, params: { id: '234' }, session: valid_session
        expect(response).to be_success
      end
      it 'assigns the requested resource as a variable' do
        allow(resource_class).to receive(:find).with('234').and_return(resource)
        get :show, params: { id: '234' }, session: valid_session
        expect(assigns(resource_symbol)).to eq(resource)
      end
      context 'with a missing id' do
        it 'gives a 404 (missing) response' do
          get :show, params: { id: bad_resource_id }, session: valid_session
          expect(response.status).to eq 404
        end
      end
      context 'with an invalid id' do
        it 'gives a 404 response' do
          get :show, params: { id: 'invalid' }, session: valid_session
          expect(response.status).to eq 404
        end
      end
    end

    describe 'GET new' do
      it 'returns a success response' do
        get :new, params: {}, session: valid_session
        aggregate_failures do
          expect(response).to be_success
          # assigns a newly created but unsaved resource
          expect(assigns(resource_symbol)).to be_a_new(resource_class)
        end
      end
    end

    describe 'POST create' do
      context 'with valid params' do
        def perform_create(attrs)
          post :create, params: { resource_symbol => attrs }, session: valid_session
        end
        it 'creates a new resource' do
          # performing the post adds a resource
          expect { perform_create(new_attributes) }.to change(resource_class, :count).by(1)
          resource = assigns(resource_symbol)
          aggregate_failures do
            expect(resource).to be_a(resource_class)
            expect(resource).to be_persisted
            # assigns all the specified values
            expected_new_values.each do |field, expected_value|
              expect(resource.public_send(field)).to eq expected_value
            end
            # redirects to the created resource
            expect(response).to redirect_to(resource)
            # sets a notice
            expect(request.flash.__send__(:now_is_loaded?)).to be_falsey
            expect(request.flash[:notice]).to match(/created/i)
            # does not set an alert
            expect(request.flash[:alert]).to be_blank
          end
        end
      end

      context 'with invalid params' do
        it 'returns a success response' do
          post :create, params: { resource_symbol => invalid_attributes }, session: valid_session
          resource = assigns(resource_symbol)
          aggregate_failures do
            # returns a success response
            expect(response).to be_success
            # assigns a newly created but unsaved resource
            expect(resource).to be_a_new(resource_class)
            expect(resource).not_to be_persisted
            # re-renders the 'new' template
            expect(response).to render_template('new')
            # assigns errors for the invalid fields
            errors = resource.errors
            invalid_create_error_symbols.each do |error_field|
              expect(errors[error_field].first).to be_present
            end
            # does not set a notice
            expect(request.flash[:notice]).to be_blank
            # sets an alert that the business failed to be created
            expect(request.flash.__send__(:now_is_loaded?)).to be_truthy
            expect(request.flash.now[:alert]).to match(/could not/i)
          end
        end
      end
    end

    describe 'GET edit' do
      it 'returns a success response' do
        allow(resource_class).to receive(:find).with('123').and_return(resource)
        get :edit, params: { id: '123' }, session: valid_session
        aggregate_failures do
          expect(response).to be_success
          expect(assigns(resource_symbol)).to eq(resource)
        end
      end
    end

    describe 'PUT update' do
      let(:resource) { $resource = resource_class.create!(valid_attributes) }

      context 'with valid params' do
        it 'updates the requested resource' do
          put(
            :update,
            params: { id: resource.to_param, resource_symbol => update_attributes },
            session: valid_session
          )
          resource = assigns(resource_symbol) # .reload
          aggregate_failures do
            expect(resource).to be_a(resource_class)
            # updates all the specified values
            expected_updated_values.each do |field, expected_value|
              expect(resource.public_send(field)).to eq expected_value
            end
            # redirects to the resource
            expect(response).to redirect_to(resource)
            # sets a notice that the resource was updated
            expect(request.flash.__send__(:now_is_loaded?)).to be_falsey
            expect(request.flash[:notice]).to match(/updated/i)
            # does not set an alert
            expect(request.flash[:alert]).to be_blank
          end
        end
      end

      context 'with invalid params' do
        it_behaves_like 'an invalid update'
      end
    end

    describe 'GET delete' do
      it 'returns a success response' do
        allow(resource_class).to receive(:find).with('567').and_return(resource)
        get :delete, params: { id: '567' }, session: valid_session
        aggregate_failures do
          expect(response).to be_success
          expect(assigns(resource_symbol)).to eq(resource)
        end
      end
    end

    describe 'DELETE destroy' do
      let(:resource) { create(resource_symbol) }
      def perform_destroy(resource)
        delete :destroy, params: { id: resource.to_param }, session: valid_session
      end
      it 'destroys the requested resource' do
        resource
        # destroys the requested resource
        expect { perform_destroy(resource) }.to change(resource_class, :count).by(-1)
        aggregate_failures do
          # redirects to the resources list
          expect(response).to redirect_to(resources_url)
          # notifies the user that the resource was deleted
          expect(request.flash.__send__(:now_is_loaded?)).to be_falsey
          expect(request.flash[:notice]).to match(/deleted/)
        end
      end
    end
  end

  context 'restricted by' do
    let(:resource) { @resource ||= create(resource_symbol) }

    context 'not signed in' do
      describe 'GET index' do
        it 'returns an unauthorized response' do
          get :index, params: {}, session: {}
          expect(response.status).to eq 401
          expect(response).to render_template('routing/login_required')
        end
      end

      describe 'GET show' do
        it 'returns an unauthorized response' do
          get :show, params: { id: resource.to_param }, session: {}
          expect(response.status).to eq 401
          expect(response).to render_template('routing/login_required')
        end
      end

      describe 'GET new' do
        it 'returns an unauthorized response' do
          get :new, params: {}, session: {}
          expect(response.status).to eq 401
          expect(response).to render_template('routing/login_required')
        end
      end

      describe 'POST create' do
        it 'returns an unauthorized response' do
          post :create, params: { resource_symbol => new_attributes }, session: {}
          expect(response.status).to eq 401
          expect(response).to render_template('routing/login_required')
        end
      end

      describe 'GET edit' do
        it 'returns an unauthorized response' do
          get :edit, params: { id: resource.to_param }, session: {}
          expect(response.status).to eq 401
          expect(response).to render_template('routing/login_required')
        end
      end

      describe 'PUT update' do
        it 'returns an unauthorized response' do
          put(
            :update,
            params: { id: resource.to_param, resource_symbol => update_attributes }, session: {}
          )
          expect(response.status).to eq 401
          expect(response).to render_template('routing/login_required')
        end
      end

      describe 'GET delete' do
        it 'returns an unauthorized response' do
          get :delete, params: { id: resource.to_param }, session: {}
          expect(response.status).to eq 401
          expect(response).to render_template('routing/login_required')
        end
      end

      describe 'DELETE destroy' do
        it 'returns an unauthorized response' do
          delete :destroy, params: { id: resource.to_param }, session: {}
          expect(response.status).to eq 401
          expect(response).to render_template('routing/login_required')
        end
      end
    end

    context 'signed in without authority' do
      before(:all) do
        @unauthorized_user = create(:confirmed_user)
      end

      let(:unauthorized_user) { @unauthorized_user }

      before(:each) do
        sign_in unauthorized_user
      end
      after(:each) do
        sign_out unauthorized_user
      end

      describe 'GET index' do
        it 'returns an unauthorized response' do
          get :index, params: {}, session: {}
          expect(response.status).to eq 403
          expect(response).to render_template('routing/unauthorized')
        end
      end

      describe 'GET show' do
        it 'returns an unauthorized response' do
          get :show, params: { id: resource.to_param }, session: {}
          expect(response.status).to eq 403
          expect(response).to render_template('routing/unauthorized')
        end
      end

      describe 'GET new' do
        it 'returns an unauthorized response' do
          get :new, params: {}, session: {}
          expect(response.status).to eq 403
          expect(response).to render_template('routing/unauthorized')
        end
      end

      describe 'POST create' do
        it 'returns an unauthorized response' do
          post :create, params: { resource_symbol => new_attributes }, session: {}
          expect(response.status).to eq 403
          expect(response).to render_template('routing/unauthorized')
        end
      end

      describe 'GET edit' do
        it 'returns an unauthorized response' do
          get :edit, params: { id: resource.to_param }, session: {}
          expect(response).to render_template('routing/unauthorized')
          expect(response.status).to eq 403
        end
      end

      describe 'PUT update' do
        it 'returns an unauthorized response' do
          put(
            :update,
            params: { id: resource.to_param, resource_symbol => update_attributes }, session: {}
          )
          expect(response.status).to eq 403
          expect(response).to render_template('routing/unauthorized')
        end
      end

      describe 'GET delete' do
        it 'returns an unauthorized response' do
          get :delete, params: { id: resource.to_param }, session: {}
          expect(response.status).to eq 403
          expect(response).to render_template('routing/unauthorized')
        end
      end

      describe 'DELETE destroy' do
        it 'returns an unauthorized response' do
          delete :destroy, params: { id: resource.to_param }, session: {}
          expect(response.status).to eq 403
          expect(response).to render_template('routing/unauthorized')
        end
      end
    end
  end
end
