require 'rails_helper'

# The following must be defined using “let(:something)” syntax
# in the specs that are referencing these shared examples:
# REQUIRED:
# - resource_class parameter: Class
# - new_attributes: hash;
#   'Field Name' => 'value to enter',
#   'Field Name' => { method: :method, value: 'value' }
# - expected_new_values: array of hashes (selector: nil|'css selector', text: 'expected value')
# - update_attributes: hash (different values than valid_attributes)
# - expected_updated_values: hash (keys are resource field names, values are the values)
# - invalid_attributes: hash; ('Field Name' => 'value to enter')
#   'Field Name => 'value to enter', 'Field Name' => { method: :method, params: params_hash}
# - invalid_attribute_keys: array of symbols
# OPTIONAL:
# - omit_feature_actions: hash of symbol-keyed actions (set value to true).
#   e.g., “{ new: true }” would cause the new actions to be skipped
# - descriptor_method: method to call on a resource to get its “descriptor” in list (index) views
# - invalid_edit_attributes: defaults to invalid_attributes
# - invalid_edit_attribute_keys: defaults to invalid_attribute_keys

RSpec.shared_context 'feature user setup' do |resource_area|
  before(:all) do
    User.where(email: 'unauthorized@feature.tld').first || create(
      :confirmed_user,
      name: 'Unauthorized Feature', email: 'unauthorized@feature.tld', password: 'password'
    )
    signin_user = User.where(email: 'signin@feature.tld').first || create(
      :confirmed_user,
      name: 'Signin Feature', email: 'signin@feature.tld', password: 'password'
    )
    signin_role = create(:role_with_full_access, areas: [resource_area], name: 'Signin Role')
    create(:authority, user: signin_user, role: signin_role)
  end
end

def fill_in_fields(attributes: {})
  # e.g., 'Field Name' => { method: :check, params: nil }
  # or 'Field Name' => 'some text'
  attributes.each do |field_name, action|
    if action.is_a? String
      fill_in field_name, with: action
    else
      case action[:method]
      when :select
        select action[:value], from: field_name
      else
        if action[:params]
          public_send(action[:method], field_name, action[:params])
        else
          public_send(action[:method], field_name)
        end
      end
    end
  end
end

def expect_values(expected_values)
  expected_values.each do |expectation|
    selector = expectation[:selector]
    if selector
      expect(page).to have_selector(selector, text: expectation[:text])
    else
      expect(page).to have_text(expectation[:text])
    end
  end
end

def signin_with_user(email: 'signin@feature.tld', password: 'password')
  visit new_user_session_url
  fill_in 'user_email', with: email
  fill_in 'user_password', with: password
  click_button 'Sign in'
end

RSpec.shared_examples 'resource feature' do |resource_class|
  let(:omit_feature_actions) { {} }
  let(:resource_name) { resource_class.name }
  let(:resource_symbol) { resource_name.underscore.to_sym }
  let(:resources_symbol) { resource_name.underscore.pluralize.to_sym }
  let(:resources_title) { resource_name.titleize }
  let(:resources_url) { "/#{resources_symbol}" }
  let(:invalid_edit_attributes) { invalid_attributes }
  let(:invalid_edit_attribute_keys) { invalid_attribute_keys }
  let(:descriptor_method) { :descriptor }
  let(:resource) { create(resource_symbol) }

  context 'with authorized user signed in' do
    before(:each) do
      signin_with_user
    end

    scenario 'Index' do
      resources = create_list(resource_symbol, 3)
      visit resources_url
      expect(page).to have_valid_html
      resource = resources[0]
      expect(page).to have_selector(
        "a[href='#{resources_url}/#{resource.to_param}']",
        text: resource.public_send(descriptor_method)
      )
      expect(page).to have_text(resources_title)
    end

    scenario 'New' do
      unless omit_feature_actions[:new]
        visit "#{resources_url}/new"
        expect(page).to have_valid_html
        fill_in_fields(attributes: new_attributes)
        click_button "Save #{resources_title}"
        expect(page).to have_valid_html
        # no errors
        expect(page).not_to have_selector('*[class="error_messages"]')
        # verify that the values have been assigned
        expect_values(expected_new_values)
      end
    end

    scenario 'New with invalid values' do
      unless omit_feature_actions[:new]
        visit "#{resources_url}/new"
        expect(page).to have_valid_html
        fill_in_fields(attributes: invalid_attributes)
        click_button "Save #{resources_title}"
        expect(page).to have_valid_html
        # list the errors
        invalid_attribute_keys.each do |attr|
          expect(page).to have_selector("*[class='error_messages'] li[id='error-#{attr}']")
        end
        # show the form again
        expect(page).to have_selector("form[action='#{resources_url}'][method='post']")
      end
    end

    # 'Show' is redundant — tested through results of new and edit

    scenario 'Edit' do
      visit "#{resources_url}/#{resource.id}/edit"
      expect(page).to have_valid_html
      fill_in_fields(attributes: update_attributes)
      click_button "Save #{resources_title}"
      expect(page).to have_valid_html
      # no errors
      expect(page).not_to have_selector('*[class="error_messages"]')
      # verify that the values have been changed
      expect_values(expected_updated_values)
    end

    scenario 'Edit with invalid values' do
      resource = create(resource_symbol)
      resource_id = resource.to_param
      visit "#{resources_url}/#{resource_id}/edit"
      expect(page).to have_valid_html
      fill_in_fields(attributes: invalid_edit_attributes)
      click_button "Save #{resources_title}"
      expect(page).to have_valid_html
      # list the errors
      invalid_edit_attribute_keys.each do |attr|
        expect(page).to have_selector("*[class='error_messages'] li[id='error-#{attr}']")
      end
      # show the form again
      expect(page).to have_selector(
        "form[action=\"#{resources_url}/#{resource_id}\"][method=\"post\"]"
      )
    end

    scenario 'Delete' do
      resource = create(resource_symbol)
      prior_count = resource_class.count
      visit "#{resources_url}/#{resource.to_param}/delete"
      expect(page).to have_valid_html
      click_button "Delete #{resources_title}"
      expect(resource_class.count).to eq(prior_count - 1)
      expect(page).to have_selector('#flash .notice', text: /#{resources_title}.+deleted/i)
    end
  end

  context 'with unauthorized user signed in' do
    before(:each) do
      signin_with_user(email: 'unauthorized@feature.tld')
    end

    let(:resource) { resource_class.first || create(resource_symbol) }

    scenario 'Index' do
      visit resources_url
      expect(page).to have_valid_html
      expect(page).to have_selector('#flash .alert', text: /not authorized/i)
      expect(page).to have_selector('h1', text: 'Restricted Access')
    end

    scenario 'New' do
      visit "#{resources_url}/new"
      expect(page).to have_valid_html
      expect(page).to have_selector('#flash .alert', text: /not authorized/i)
      expect(page).to have_selector('h1', text: 'Restricted Access')
    end

    scenario 'Edit' do
      visit "#{resources_url}/#{resource.to_param}/edit"
      expect(page).to have_valid_html
      expect(page).to have_selector('#flash .alert', text: /not authorized/i)
      expect(page).to have_selector('h1', text: 'Restricted Access')
    end

    scenario 'Delete' do
      visit "#{resources_url}/#{resource.to_param}/delete"
      expect(page).to have_valid_html
      expect(page).to have_selector('#flash .alert', text: /not authorized/i)
      expect(page).to have_selector('h1', text: 'Restricted Access')
    end
  end

  context 'not signed in' do
    let(:resource) { resource_class.first || create(resource_symbol) }

    scenario 'Index' do
      visit resources_url
      expect(page).to have_valid_html
      expect(page).to have_selector('#flash .alert', text: /sign in/i)
      expect(page).to have_selector('h1', text: 'Sign In Required')
      expect(page).to have_selector('a[href="/signin"]', text: 'Sign In')
    end

    scenario 'New' do
      visit "#{resources_url}/new"
      expect(page).to have_valid_html
      expect(page).to have_selector('#flash .alert', text: /sign in/i)
      expect(page).to have_selector('h1', text: 'Sign In Required')
      expect(page).to have_selector('a[href="/signin"]', text: 'Sign In')
    end

    scenario 'Edit' do
      visit "#{resources_url}/#{resource.to_param}/edit"
      expect(page).to have_valid_html
      expect(page).to have_selector('#flash .alert', text: /sign in/i)
      expect(page).to have_selector('h1', text: 'Sign In Required')
      expect(page).to have_selector('a[href="/signin"]', text: 'Sign In')
    end

    scenario 'Delete' do
      visit "#{resources_url}/#{resource.to_param}/delete"
      expect(page).to have_valid_html
      expect(page).to have_selector('#flash .alert', text: /sign in/i)
      expect(page).to have_selector('h1', text: 'Sign In Required')
      expect(page).to have_selector('a[href="/signin"]', text: 'Sign In')
    end
  end
end
