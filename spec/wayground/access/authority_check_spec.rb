require 'rails_helper'
require 'access/authority_check'
require 'authority'
require 'user'

RSpec.describe Wayground::Access::AuthorityCheck do
  let(:user) { User.new }
  let(:area) { 'system' }
  let(:action) { :can_read }
  let(:minimum_params) { { user: user, area: area, action: action } }

  describe 'initialization' do
    it 'should accept a user' do
      user = User.new
      checker = Wayground::Access::AuthorityCheck.new(minimum_params.merge(user: user))
      expect(checker.user).to eq user
    end
    it 'should accept an area' do
      checker = Wayground::Access::AuthorityCheck.new(minimum_params.merge(area: 'contact'))
      expect(checker.area).to eq 'contact'
    end
    it 'should accept an action' do
      checker = Wayground::Access::AuthorityCheck.new(minimum_params.merge(action: :can_create))
      expect(checker.action).to eq :can_create
    end
  end

  describe '#authority' do
    it 'should return the first authority found' do
      authorities = double
      allow(authorities).to receive(:first).and_return(:first_authority)
      checker = Wayground::Access::AuthorityCheck.new(minimum_params)
      allow(checker).to receive(:authorities).and_return(authorities)
      expect(checker.authority).to eq :first_authority
    end
  end

  describe '#authorities' do
    before(:all) do
      Authority.delete_all
      User.delete_all
      @user = create(:confirmed_user)
      @user2 = create(:confirmed_user)
      @role = create(:role, areas: ['system'], can_read: true)
      @role2 = create(:role, areas: ['system'], can_read: false)
      @role3 = create(:role, areas: ['contact'], can_read: true)
      # authorities to be found
      @authority = create(
        :authority, user: @user, role: @role, suspends_at: 1.week.ago, unsuspends_at: 1.day.ago
      )
      # authorities to be ignored
      # not yet active
      create(:authority, user: @user, role: @role, active_at: 1.week.from_now)
      # ended
      create(:authority, user: @user, role: @role, ends_at: 1.day.ago)
      # currently suspended
      create(:authority, user: @user, role: @role, suspends_at: 1.day.ago)
      # different user
      create(:authority, user: @user2, role: @role)
      # different role, same area, different actions
      create(:authority, user: @user, role: @role2)
      # different role, different area, same actions
      create(:authority, user: @user, role: @role3)
    end
    after(:all) do
      Authority.delete_all
      [@user, @user2, @role, @role2, @role3].each(&:destroy)
    end
    it 'should return just the expected authority' do
      checker = Wayground::Access::AuthorityCheck.new(
        user: @user, area: 'system', action: :can_read
      )
      expect(checker.authorities.to_a).to eq [@authority]
    end
  end
end
