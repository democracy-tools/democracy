require 'rails_helper'
require 'setup/installer'

RSpec.describe Wayground::Setup::Installer do
  before(:all) do
    Authority.delete_all
    User.delete_all
  end

  let(:user_params) do
    {
      'user' => {
        'name' => 'Build User', 'email' => 'build@user.tld',
        'password' => 'password', 'password_confirmation' => 'password'
      }
    }
  end

  describe 'initialization' do
    it 'should accept params' do
      installer = Wayground::Setup::Installer.new(:test)
      expect(installer.params).to eq :test
    end
  end

  describe '#create_initial_records' do
    it 'should create the user, role, and authority' do
      params = ActionController::Parameters.new(user_params)
      installer = Wayground::Setup::Installer.new(params)
      expect_any_instance_of(User).to receive(:save!)
      allow_any_instance_of(Role).to receive(:save!)
      allow_any_instance_of(Authority).to receive(:save!)
      # perform action
      records = installer.create_initial_records
      aggregate_failures do
        expect(records[:user]).to be_a User
        expect(records[:roles].first).to be_a Role
        expect(records[:authorities].first).to be_an Authority
      end
    end
    context 'with invalid field values' do
      let(:user_params) do
        {
          'user' => {
            'name' => '', 'email' => 'not an email',
            'password' => 'short', 'password_confirmation' => 'different'
          }
        }
      end
      it 'should raise an exception' do
        params = ActionController::Parameters.new(user_params)
        installer = Wayground::Setup::Installer.new(params)
        # perform action
        expect { installer.create_initial_records }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end
  end

  describe '#build_records' do
    it 'should build the six new records for admin' do
      params = ActionController::Parameters.new(user_params)
      installer = Wayground::Setup::Installer.new(params)
      records = installer.__send__(:build_records)
      aggregate_failures do
        expect(records[:user]).to be_a User
        # System Administrator
        role = records[:roles].first
        expect(role).to be_a Role
        authority = records[:authorities].first
        expect(authority).to be_an Authority
        expect(authority.user).to eq records[:user]
        expect(authority.role).to eq role
        # Pages Editor
        role = records[:roles].second
        expect(role).to be_a Role
        authority = records[:authorities].second
        expect(authority).to be_an Authority
        expect(authority.user).to eq records[:user]
        expect(authority.role).to eq role
      end
    end
  end

  describe '#build_user' do
    context 'with minimum valid params' do
      it 'should return a new user' do
        params = ActionController::Parameters.new(user_params)
        installer = Wayground::Setup::Installer.new(params)
        user = installer.__send__(:build_user)
        aggregate_failures do
          expect(user).to be_a User
          expect(user.valid?).to be_truthy
          expect(user.persisted?).to be_falsey
          expect(user.name).to eq 'Build User'
          expect(user.email).to eq 'build@user.tld'
          expect(user.valid_password?('password')).to be_truthy
        end
      end
    end
  end

  describe '#build_roles' do
    it 'should return a new role' do
      installer = Wayground::Setup::Installer.new(nil)
      roles = installer.__send__(:build_roles)
      aggregate_failures do
        # System Administrator
        role = roles.first
        expect(role.name).to eq 'System Administrator'
        # Pages Editor
        role = roles.second
        expect(role.name).to eq 'Pages Editor'
      end
    end
  end

  describe '#build_sysadmin_role' do
    it 'should return a new role' do
      installer = Wayground::Setup::Installer.new(nil)
      role = installer.__send__(:build_sysadmin_role)
      aggregate_failures do
        expect(role).to be_a Role
        expect(role.valid?).to be_truthy
        expect(role.persisted?).to be_falsey
        expect(role.areas).to eq ['system']
        expect(role.name).to eq 'System Administrator'
        expect(role.description).to match(/Administration/)
        expect(role.can_read).to be_truthy
        expect(role.can_create).to be_truthy
        expect(role.can_update).to be_truthy
        expect(role.can_destroy).to be_truthy
        expect(role.can_upload).to be_truthy
        expect(role.can_comment).to be_truthy
      end
    end
  end

  describe '#build_pageadmin_role' do
    it 'should return a new role' do
      installer = Wayground::Setup::Installer.new(nil)
      role = installer.__send__(:build_pageadmin_role)
      aggregate_failures do
        expect(role).to be_a Role
        expect(role.valid?).to be_truthy
        expect(role.persisted?).to be_falsey
        expect(role.areas).to eq ['page']
        expect(role.name).to eq 'Pages Editor'
        expect(role.description).to match(/static page/i)
        expect(role.can_read).to be_truthy
        expect(role.can_create).to be_truthy
        expect(role.can_update).to be_truthy
        expect(role.can_destroy).to be_truthy
        expect(role.can_upload).to be_truthy
        expect(role.can_comment).to be_truthy
      end
    end
  end

  describe '#build_authorities' do
    it 'should return a new authority' do
      installer = Wayground::Setup::Installer.new(nil)
      user = build_stubbed(:user)
      roles = [build_stubbed(:role)]
      authorities = installer.__send__(:build_authorities, user, roles)
      aggregate_failures do
        authority = authorities.first
        expect(authority).to be_an Authority
        expect(authority.valid?).to be_truthy
        expect(authority.persisted?).to be_falsey
        expect(authority.active_at).to be_an ActiveSupport::TimeWithZone
      end
    end
    it 'should use time with zone' do
      installer = Wayground::Setup::Installer.new(nil)
      user = build_stubbed(:user)
      roles = [build_stubbed(:role)]
      expect(Authority).to receive(:new).with(
        user: user, role: roles[0], active_at: instance_of(ActiveSupport::TimeWithZone)
      )
      installer.__send__(:build_authorities, user, roles)
    end
  end

  describe '#validate_all' do
    context 'with invalid user params' do
      it 'should raise an exception' do
        params = { 'user' => {} }
        params = ActionController::Parameters.new(params)
        installer = Wayground::Setup::Installer.new(params)
        expect { installer.create_initial_records }.to raise_error(
          ActiveRecord::RecordInvalid
        )
      end
    end

    context 'with blank user params' do
      it 'should raise an exception' do
        params = {}
        params = ActionController::Parameters.new(params)
        installer = Wayground::Setup::Installer.new(params)
        expect { installer.create_initial_records }.to raise_error(
          ActiveRecord::RecordInvalid
        )
      end
    end
  end

  describe '#save_all' do
    it 'should call save! on each element' do
      params = ActionController::Parameters.new(user_params)
      installer = Wayground::Setup::Installer.new(params)
      expect_any_instance_of(User).to receive(:save!)
      allow_any_instance_of(Role).to receive(:save!)
      allow_any_instance_of(Authority).to receive(:save!)
      installer.create_initial_records
      expect(installer.record_list[:user].confirmed?).to be_truthy
    end
  end
end
